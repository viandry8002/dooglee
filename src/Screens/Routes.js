import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import React, {useState, useEffect} from 'react';
import {
  SplashScreen,
  Auth,
  Verifikasi,
  UserPregnancy,
  Home,
  Blank,
  Pregnancy,
  Event,
  DtlEvent,
  Profile,
  UpdateProfile,
  ListAlamat,
  FormAlamat,
  MetodePembayaran,
  Shopping,
  DtlProduct,
  SearchProduct,
  PesananSaya,
} from './MainScreens';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Colors from '../Styles/Colors';
// import {screenWidth} from '../Styles/Styles';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import Font from '../Styles/Fonts';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {Badge} from 'react-native-elements';
import {screenWidth} from '../Variable';

const Stack = createStackNavigator();
const Tab = createMaterialBottomTabNavigator();

function HomeSS() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={Home}
        options={{
          headerStyle: {
            backgroundColor: Colors.primary,
            height: 130,
            elevation: 0,
          },
          headerTitle: () => (
            <View>
              <Text style={[Font.reguler12, {color: Colors.white}]}>
                Hai, Apa kabar👋
              </Text>
              <Text style={[Font.bold24, {color: Colors.white}]}>
                Viandry Prasiswandava
              </Text>
            </View>
          ),
          headerLeft: false,
        }}
      />
    </Stack.Navigator>
  );
}

function EventSS() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Event"
        component={Event}
        options={{
          headerLeft: false,
          headerTitle: () => (
            <Text style={[Font.bold14, {color: Colors.primary}]}>Event</Text>
          ),
        }}
      />
    </Stack.Navigator>
  );
}

function ShoppingSS() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Shopping"
        component={Shopping}
        options={({navigation}) => ({
          headerLeft: false,
          headerTitle: () => (
            <TouchableOpacity
              onPress={() => navigation.navigate('SearchProduct')}
              style={{
                backgroundColor: Colors.container,
                height: 34,
                borderRadius: 100,
                width: screenWidth - 70,
                paddingHorizontal: 8,
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Image
                source={require('../Assets/icons/search.png')}
                style={{width: 18, height: 18}}
              />
              <Text
                style={[
                  Font.reguler12,
                  {color: Colors.placeholder, marginLeft: 16},
                ]}>
                Cari Sesuatu...
              </Text>
            </TouchableOpacity>
          ),
          headerRight: () => (
            <View style={{marginRight: 16}}>
              <Image
                source={require('../Assets/icons/belanja.png')}
                style={{width: 24, height: 24}}
              />
              <Badge
                value={3}
                containerStyle={{position: 'absolute', top: -7, left: 12}}
                badgeStyle={{
                  backgroundColor: Colors.error,
                  borderColor: Colors.white,
                  borderWidth: 1,
                }}
              />
            </View>
          ),
          headerStyle: {
            elevation: 0,
          },
        })}
      />
    </Stack.Navigator>
  );
}

function ProfilSS() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Profile"
        component={Profile}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
}

const MainTabNavigation = () => {
  return (
    <Tab.Navigator
      // initialRouteName={'Akun'}
      initialRouteName={'HomeSS'}
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconImage;

          if (route.name === 'HomeSS') {
            iconImage = focused
              ? require('../Assets/icons/Home_fill.png')
              : require('../Assets/icons/Home.png');
          } else if (route.name === 'EventSS') {
            iconImage = focused
              ? require('../Assets/icons/Event_fill.png')
              : require('../Assets/icons/Event.png');
          } else if (route.name === 'ShoppingSS') {
            iconImage = focused
              ? require('../Assets/icons/Shop_fill.png')
              : require('../Assets/icons/Shop_outline.png');
          } else if (route.name === 'ProfilSS') {
            iconImage = focused
              ? require('../Assets/icons/User_fill.png')
              : require('../Assets/icons/User.png');
          }
          return (
            <Image
              source={iconImage}
              style={{width: 24, height: 24}}
              resizeMode={'contain'}
            />
          );
        },
      })}
      activeColor={Colors.primary}
      inactiveColor={Colors.placeholder}
      barStyle={{
        backgroundColor: Colors.white,
      }}
      shifting={false}>
      <Tab.Screen
        name="HomeSS"
        component={HomeSS}
        options={{title: 'Beranda'}}
      />
      <Tab.Screen
        name="EventSS"
        component={EventSS}
        options={{title: 'Event'}}
      />
      <Tab.Screen
        name="ShoppingSS"
        component={ShoppingSS}
        options={{title: 'Shopping'}}
      />
      <Tab.Screen
        name="ProfilSS"
        component={ProfilSS}
        options={{title: 'Profile'}}
      />
    </Tab.Navigator>
  );
};

const MainNavigation = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Auth"
        component={Auth}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Verifikasi"
        component={Verifikasi}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="UserPregnancy"
        component={UserPregnancy}
        options={({navigation}) => ({
          headerLeft: () => (
            <AntDesign
              name="left"
              size={16}
              color={Colors.primary}
              style={{marginLeft: 16}}
              onPress={() => navigation.goBack()}
            />
          ),
          headerTitle: () => (
            <Text style={[Font.bold14, {color: Colors.primary}]}>
              Tambah Anak
            </Text>
          ),
          headerStyle: {
            elevation: 0,
          },
        })}
      />
      <Stack.Screen
        name="Home"
        component={MainTabNavigation}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Pregnancy"
        component={Pregnancy}
        options={({navigation}) => ({
          headerLeft: () => (
            <AntDesign
              name="left"
              size={16}
              color={Colors.primary}
              style={{marginLeft: 16}}
              onPress={() => navigation.goBack()}
            />
          ),
          headerTitle: () => (
            <Text style={[Font.bold14, {color: Colors.primary}]}>
              Pregnancy
            </Text>
          ),
          headerStyle: {
            elevation: 0,
          },
        })}
      />
      <Stack.Screen
        name="DtlEvent"
        component={DtlEvent}
        options={({navigation}) => ({
          headerLeft: () => (
            <AntDesign
              name="left"
              size={16}
              color={Colors.primary}
              style={{marginLeft: 16}}
              onPress={() => navigation.goBack()}
            />
          ),
          headerTitle: () => (
            <Text style={[Font.bold14, {color: Colors.primary}]}>
              Event Detail
            </Text>
          ),
        })}
      />
      <Stack.Screen
        name="UpdateProfile"
        component={UpdateProfile}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="ListAlamat"
        component={ListAlamat}
        options={({navigation}) => ({
          headerLeft: () => (
            <AntDesign
              name="left"
              size={16}
              color={Colors.primary}
              style={{marginLeft: 16}}
              onPress={() => navigation.goBack()}
            />
          ),
          headerTitle: () => (
            <Text style={[Font.bold14, {color: Colors.primary}]}>
              Pilih Alamat
            </Text>
          ),
        })}
      />
      <Stack.Screen
        name="FormAlamat"
        component={FormAlamat}
        options={({navigation}) => ({
          headerLeft: () => (
            <AntDesign
              name="left"
              size={16}
              color={Colors.primary}
              style={{marginLeft: 16}}
              onPress={() => navigation.goBack()}
            />
          ),
          headerTitle: () => (
            <Text style={[Font.bold14, {color: Colors.primary}]}>
              Tambah alamat
            </Text>
          ),
        })}
      />
      <Stack.Screen
        name="MetodePembayaran"
        component={MetodePembayaran}
        options={({navigation}) => ({
          headerLeft: () => (
            <AntDesign
              name="left"
              size={16}
              color={Colors.primary}
              style={{marginLeft: 16}}
              onPress={() => navigation.goBack()}
            />
          ),
          headerTitle: () => (
            <Text style={[Font.bold14, {color: Colors.primary}]}>
              Pilih Metode Pembayaran
            </Text>
          ),
        })}
      />

      <Stack.Screen
        name="DtlProduct"
        component={DtlProduct}
        options={({navigation}) => ({
          headerLeft: () => (
            <AntDesign
              name="left"
              size={16}
              color={Colors.primary}
              style={{marginLeft: 16}}
              onPress={() => navigation.goBack()}
            />
          ),
          headerTitle: () => (
            <TouchableOpacity
              onPress={() => navigation.navigate('SearchProduct')}
              style={{
                backgroundColor: Colors.container,
                height: 34,
                borderRadius: 100,
                width: screenWidth - 100,
                paddingHorizontal: 8,
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Image
                source={require('../Assets/icons/search.png')}
                style={{width: 18, height: 18}}
              />
              <Text
                style={[
                  Font.reguler12,
                  {color: Colors.placeholder, marginLeft: 16},
                ]}>
                Cari Sesuatu...
              </Text>
            </TouchableOpacity>
          ),
          headerRight: () => (
            <View style={{marginRight: 16}}>
              <Image
                source={require('../Assets/icons/belanja.png')}
                style={{width: 24, height: 24}}
              />
              <Badge
                value={3}
                containerStyle={{position: 'absolute', top: -7, left: 12}}
                badgeStyle={{
                  backgroundColor: Colors.error,
                  borderColor: Colors.white,
                  borderWidth: 1,
                }}
              />
            </View>
          ),
        })}
      />

      <Stack.Screen
        name="SearchProduct"
        component={SearchProduct}
        options={{headerShown: false}}
      />

      <Stack.Screen
        name="PesananSaya"
        component={PesananSaya}
        options={({navigation}) => ({
          headerLeft: () => (
            <AntDesign
              name="left"
              size={16}
              color={Colors.primary}
              style={{marginLeft: 16}}
              onPress={() => navigation.goBack()}
            />
          ),
          headerTitle: () => (
            <Text style={[Font.bold14, {color: Colors.primary}]}>
              Pesanan Saya
            </Text>
          ),
          headerStyle: {
            elevation: 0,
          },
        })}
      />
    </Stack.Navigator>
  );
};

const Routes = () => {
  // const [isLoading, setIsLoading] = useState(true);

  // useEffect(() => {
  //   setTimeout(() => {
  //     setIsLoading(!isLoading);
  //   }, 3000);
  // }, []);

  // if (isLoading) {
  //   return <SplashScreen />;
  // }
  return (
    <NavigationContainer>
      <MainNavigation />
    </NavigationContainer>
  );
};

export default Routes;

const styles = StyleSheet.create({});
