import {StyleSheet, Text, View} from 'react-native';
import React, {useState} from 'react';
import Colors from '../../Styles/Colors';
import Font from '../../Styles/Fonts';

const Trimester = ({percent, width, showTrimester}) => {
  //   const [percent, setPercent] = useState(0);
  const [trisemester, setTrisemester] = useState(1);

  return (
    <View style={{flexDirection: 'row'}}>
      <View>
        <View
          style={{
            backgroundColor: Colors.primaryShading,
            height: 13,
            borderTopLeftRadius: 100,
            borderBottomLeftRadius: 100,
            width: width,
            marginRight: 4,
            marginVertical: 4,
          }}>
          {trisemester >= 1 ? (
            <View
              style={{
                backgroundColor: Colors.primary,
                width: '50%',
                height: '100%',
                borderTopLeftRadius: 100,
                borderBottomLeftRadius: 100,
              }}
            />
          ) : (
            false
          )}
        </View>
        {showTrimester === true ? (
          <Text style={[Font.reguler12, {color: Colors.placeholder}]}>
            Trimester 1
          </Text>
        ) : (
          false
        )}
      </View>
      <View>
        <View
          style={{
            backgroundColor: Colors.primaryShading,
            height: 13,
            width: width,
            marginRight: 4,
            marginVertical: 4,
          }}>
          {trisemester > 1 ? (
            <View
              style={{
                backgroundColor: Colors.primary,
                width: '50%',
                height: '100%',
              }}
            />
          ) : (
            false
          )}
        </View>
        {showTrimester === true ? (
          <Text style={[Font.reguler12, {color: Colors.placeholder}]}>
            Trimester 2
          </Text>
        ) : (
          false
        )}
      </View>
      <View>
        <View
          style={{
            backgroundColor: Colors.primaryShading,
            height: 13,
            borderTopRightRadius: 100,
            borderBottomRightRadius: 100,
            width: width,
            marginVertical: 4,
          }}>
          {trisemester > 2 ? (
            <View
              style={{
                backgroundColor: Colors.primary,
                width: '50%',
                height: '100%',
                //   borderTopRightRadius: 100,
                //   borderBottomRightRadius: 100,
              }}
            />
          ) : (
            false
          )}
        </View>
        {showTrimester === true ? (
          <Text style={[Font.reguler12, {color: Colors.placeholder}]}>
            Trimester 3
          </Text>
        ) : (
          false
        )}
      </View>
    </View>
  );
};

export default Trimester;

const styles = StyleSheet.create({});
