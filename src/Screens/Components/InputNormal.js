import {StyleSheet, View, Text} from 'react-native';
import React from 'react';
import {Input} from 'react-native-elements';
import Font from '../../Styles/Fonts';
import Colors from '../../Styles/Colors';

const InputNormal = ({label, title, val}) => {
  return (
    <View style={{marginTop: 16}}>
      <Text style={Font.bold12}>{label}</Text>
      <Input
        placeholder={title}
        style={[Font.reguler12, {color: Colors.black}]}
        inputContainerStyle={{borderBottomWidth: 0}}
        containerStyle={{
          height: 48,
          marginTop: 10,
          borderWidth: 1,
          borderRadius: 100,
          borderColor: Colors.container,
        }}
        value={val}
      />
    </View>
  );
};

export default InputNormal;

const styles = StyleSheet.create({});
