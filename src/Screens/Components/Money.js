import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {TextMask} from 'react-native-masked-text';

const Money = ({value, styling}) => {
  return (
    <TextMask
      type={'money'}
      options={{
        precision: 0,
        separator: '.',
        delimiter: '.',
        unit: 'Rp',
        suffixUnit: '',
      }}
      value={value}
      style={styling}
      numberOfLines={1}
    />
  );
};

export default Money;

const styles = StyleSheet.create({});
