import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import Colors from '../../Styles/Colors';
import Font from '../../Styles/Fonts';

const BtnPrimary = ({title, pressed}) => {
  return (
    <TouchableOpacity
      style={{
        backgroundColor: Colors.primary,
        height: 48,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 100,
      }}
      onPress={pressed}>
      <Text style={[Font.reguler14, {color: Colors.white}]}>{title}</Text>
    </TouchableOpacity>
  );
};

export default BtnPrimary;

const styles = StyleSheet.create({});
