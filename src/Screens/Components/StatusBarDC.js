import {StyleSheet, StatusBar} from 'react-native';
import React from 'react';

const StatusBarDC = () => {
  return (
    <StatusBar
      backgroundColor="transparent"
      translucent
      barStyle="dark-content"
    />
  );
};

export default StatusBarDC;

const styles = StyleSheet.create({});
