import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import Colors from '../../Styles/Colors';
import Font from '../../Styles/Fonts';

const BtnOutlineSM = ({title, pressed}) => {
  return (
    <TouchableOpacity
      style={{
        borderWidth: 1,
        borderRadius: 100,
        borderColor: Colors.primary,
        width: 73,
        height: 48,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 100,
        marginRight: 8,
      }}
      onPress={pressed}>
      <Text style={[Font.reguler14, {color: Colors.primary}]}>{title}</Text>
    </TouchableOpacity>
  );
};

export default BtnOutlineSM;

const styles = StyleSheet.create({});
