import {StyleSheet, Image} from 'react-native';
import React from 'react';
import {Input} from 'react-native-elements';
import Font from '../../Styles/Fonts';
import Colors from '../../Styles/Colors';

const InputComponent = ({title, icon, secure}) => {
  return (
    <Input
      placeholder={title}
      leftIcon={<Image source={icon} style={{width: 24, height: 24}} />}
      style={[Font.reguler12, {color: Colors.placeholder}]}
      inputContainerStyle={{borderBottomWidth: 0}}
      containerStyle={{
        height: 48,
        marginTop: 16,
        borderWidth: 1,
        borderRadius: 100,
        borderColor: Colors.container,
      }}
      secureTextEntry={secure}
    />
  );
};

export default InputComponent;

const styles = StyleSheet.create({});
