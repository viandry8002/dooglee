import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  Image,
  TouchableOpacity,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import StatusBarDC from '../../Components/StatusBarDC';
import InputComponent from '../../Components/InputComponent';
import BtnPrimary from '../../Components/BtnPrimary';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {Input} from 'react-native-elements';
import BtnOutlineSM from '../../Components/BtnOutlineSM';
import InputNormal from '../../Components/InputNormal';
import Trimester from '../../Components/Trimester';
// import * as Animatable from 'react-native-animatable';

const index = ({navigation, route}) => {
  const [form, setForm] = useState('Hamil');
  const [gender, setGender] = useState('Laki-laki');

  useEffect(() => {
    if (route.params.type === 'Regist') {
      navigation.setOptions({headerShown: false});
    }
  }, []);

  const renderForm = title => {
    return (
      <View style={{flex: 1}}>
        <TouchableOpacity
          style={{alignItems: 'center', paddingVertical: 8}}
          onPress={() => setForm(title)}>
          <Text
            style={[
              Font.reguler12,
              form === title
                ? {color: Colors.primary}
                : {color: Colors.placeholder},
            ]}>
            {title}
          </Text>
        </TouchableOpacity>
        <View
          style={
            form === title
              ? {
                  borderBottomColor: Colors.primary,
                  borderBottomWidth: 1,
                }
              : {
                  borderBottomColor: Colors.placeholder,
                  borderBottomWidth: 0.5,
                }
          }
        />
      </View>
    );
  };

  const renderDate = title => {
    return (
      <Input
        placeholder={title}
        leftIcon={
          <Image
            source={require('../../../Assets/icons/Date_range_fill.png')}
            style={{width: 24, height: 24}}
          />
        }
        style={[Font.reguler12, {color: Colors.placeholder}]}
        inputContainerStyle={{borderBottomWidth: 0}}
        containerStyle={styles.inputContainer}
        rightIcon={
          <FontAwesome name="chevron-right" size={10} color={Colors.gray2} />
        }
      />
    );
  };

  const renderGender = (title, image) => {
    return (
      <View style={{flex: 1}}>
        <TouchableOpacity
          onPress={() => setGender(title)}
          style={gender === title ? styles.genderTrue : styles.genderFalse}>
          <Image
            source={image}
            style={{width: 36, height: 36, marginRight: 16}}
          />
          <Text style={Font.reguler12}>{title}</Text>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: Colors.secondary}}>
      <StatusBarDC />
      <View style={{flex: 1}}>
        <View style={{height: 44}} />
        <View style={{alignItems: 'center'}}>
          <Image
            source={require('../../../Assets/images/pregnancy.png')}
            style={{width: 170, height: 170}}
            resizeMode={'contain'}
          />
        </View>
        <View style={{height: 16}} />
        {/* body */}
        <View style={styles.wrapBody}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
            }}>
            {renderForm('Hamil')}
            {renderForm('Sudah Lahir')}
          </View>

          {/* form */}
          {form === 'Hamil' ? (
            <View style={{margin: 16}}>
              <Text style={Font.bold16}>Menghitung Kelahiran Si Kecil</Text>
              <View style={{height: 16}} />
              <Text style={[Font.reguler12, {color: Colors.placeholder}]}>
                Men-tracking usia janin untuk menentukan produk rekomendasi
                dooglee untuk kebaikan bumil dan si kecil
              </Text>
              <View style={{height: 8}} />
              {renderDate('Hari Pertama Haid Terakhir')}
              <View style={{height: 16}} />
              {/* elevation content */}
              <View style={styles.wrapElevation}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}>
                  <View>
                    <Text style={[Font.bold12, {color: Colors.primary}]}>
                      Perkiraan Tanggal Kelahiran
                    </Text>
                    <Text style={[Font.reguler16]}>08 Oktober 2022</Text>
                  </View>
                  <Image
                    source={require('../../../Assets/images/janin.png')}
                    style={{width: 80, height: 80}}
                    resizeMode={'contain'}
                  />
                </View>
                <View>
                  <Text style={[Font.bold12, {color: Colors.primary}]}>
                    Usia Kehamilan
                  </Text>
                  <Text style={Font.reguler12}>7 Minggu 3 Hari</Text>
                  <Trimester width={103} showTrimester={true} />
                </View>
              </View>
            </View>
          ) : (
            <View style={{margin: 16}}>
              <Text style={Font.bold16}>
                Produk Yang Berguna Untuk Si Kecil
              </Text>
              <View style={{height: 16}} />
              <Text style={[Font.reguler12, {color: Colors.placeholder}]}>
                Untuk kami mengetahui produk yang cocok untuk si kecil, isi data
                di bawah ini ya!
              </Text>
              <InputNormal label={'Nama Anak'} title={'Nama anak kamu'} />
              <View style={{height: 16}} />
              <Text style={Font.bold12}>Tanggal Kelahiran Anak</Text>
              {renderDate('Tanggal Lahir')}
              <View style={{height: 16}} />
              <Text style={Font.bold12}>Jenis Kelamin</Text>
              <View style={{height: 10}} />
              <View
                style={{
                  flexDirection: 'row',
                }}>
                {renderGender(
                  'Laki-laki',
                  require('../../../Assets/images/boy.png'),
                )}
                <View style={{flex: 0.1}} />
                {renderGender(
                  'Perempuan',
                  require('../../../Assets/images/girl.png'),
                )}
              </View>
            </View>
          )}
        </View>
        {route.params.type === 'Regist' ? (
          <View style={styles.absoluteFooter1}>
            <BtnOutlineSM title={'Lewati'} />
            <View style={{flex: 1}}>
              <BtnPrimary
                title={'Selanjutnya'}
                pressed={() => navigation.navigate('Home')}
              />
            </View>
          </View>
        ) : (
          <View style={styles.absoluteFooter2}>
            <BtnPrimary title={'Simpan'} />
          </View>
        )}
      </View>
    </SafeAreaView>
  );
};

export default index;

const styles = StyleSheet.create({
  wrapBody: {
    flex: 1,
    backgroundColor: Colors.white,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingVertical: 16,
  },
  inputContainer: {
    height: 48,
    marginTop: 8,
    borderWidth: 1,
    borderRadius: 100,
    borderColor: Colors.container,
  },
  absoluteFooter1: {
    flexDirection: 'row',
    backgroundColor: Colors.white,
    padding: 16,
    elevation: 10,
  },
  absoluteFooter2: {
    backgroundColor: Colors.white,
    padding: 16,
    elevation: 10,
  },
  wrapElevation: {
    backgroundColor: Colors.white,
    paddingHorizontal: 16,
    height: 162,
    borderRadius: 15,
    elevation: 5,
  },
  genderTrue: {
    backgroundColor: Colors.lightPrimary,
    borderWidth: 1,
    borderRadius: 100,
    borderColor: Colors.success,
    height: 52,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  genderFalse: {
    backgroundColor: Colors.white,
    borderWidth: 1,
    borderRadius: 100,
    borderColor: Colors.container,
    height: 52,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
});
