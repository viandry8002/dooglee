import {StyleSheet, View, Image, Animated} from 'react-native';
import React, {useEffect, useRef} from 'react';
import Colors from '../../../Styles/Colors';
import StatusBarDC from '../../Components/StatusBarDC';

const index = () => {
  const fadeIn = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    Animated.timing(fadeIn, {
      toValue: 1,
      duration: 3000,
      useNativeDriver: false,
    }).start();
  }, [fadeIn]);

  return (
    <View style={styles.wrap}>
      <StatusBarDC />
      <Animated.View style={{opacity: fadeIn}}>
        <Image
          source={require('../../../Assets/images/logo1.png')}
          style={{width: 121, height: 189}}
          resizeMode={'contain'}
        />
      </Animated.View>
    </View>
  );
};

export default index;

const styles = StyleSheet.create({
  wrap: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.secondary,
  },
});
