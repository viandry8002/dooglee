import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Image,
  RefreshControl,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import Colors from '../../../Styles/Colors';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Font from '../../../Styles/Fonts';
import {screenWidth} from '../../../Variable';
import ShimmerPlaceHolder from 'react-native-shimmer-placeholder';
import LinearGradient from 'react-native-linear-gradient';
import StatusBarDC from '../../Components/StatusBarDC';

const dataEvent = [
  {
    id: 1,
    image: require('../../../Assets/images/banner.png'),
    title: 'Healty Mom’s Yoga Prenatal Class',
    date: '23 April 2022, 08:00 - 17:00',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Senectus nunc nec nunc mollis.......',
  },
  {
    id: 2,
    image: require('../../../Assets/images/banner.png'),
    title: 'Healty Mom’s Yoga Prenatal Class',
    date: '23 April 2022, 08:00 - 17:00',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Senectus nunc nec nunc mollis.......',
  },
  {
    id: 3,
    image: require('../../../Assets/images/banner.png'),
    title: 'Healty Mom’s Yoga Prenatal Class',
    date: '23 April 2022, 08:00 - 17:00',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Senectus nunc nec nunc mollis.......',
  },
  {
    id: 4,
    image: require('../../../Assets/images/banner.png'),
    title: 'Healty Mom’s Yoga Prenatal Class',
    date: '23 April 2022, 08:00 - 17:00',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Senectus nunc nec nunc mollis.......',
  },
  {
    id: 5,
    image: require('../../../Assets/images/banner.png'),
    title: 'Healty Mom’s Yoga Prenatal Class',
    date: '23 April 2022, 08:00 - 17:00',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Senectus nunc nec nunc mollis.......',
  },
];

const index = ({navigation}) => {
  const [refreshing, setRefreshing] = useState(false);
  const [shimmer, setShimmer] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setShimmer(false);
    }, 2000);
  }, []);

  const onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  };

  const renderEvent = ({item}) => {
    return (
      <TouchableOpacity
        style={{
          marginHorizontal: 16,
          marginVertical: 4,
          borderRadius: 15,
          height: 256,
          width: screenWidth - 32,
          backgroundColor: Colors.white,
          elevation: 4,
          flex: 0.5,
        }}
        onPress={() => navigation.navigate('DtlEvent')}>
        <Image
          source={item.image}
          style={{
            width: screenWidth - 32,
            height: 159,
            borderTopLeftRadius: 15,
            borderTopRightRadius: 15,
            resizeMode: 'cover',
          }}
        />
        <View style={{flex: 1, margin: 8, justifyContent: 'space-between'}}>
          <Text style={Font.bold14}>{item.title}</Text>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <AntDesign
              name="clockcircleo"
              size={16}
              color={Colors.placeholder}
            />
            <Text
              style={[
                Font.reguler12,
                {color: Colors.placeholder, marginLeft: 8},
              ]}>
              {item.date}
            </Text>
          </View>
          <Text
            style={[Font.reguler12, {color: Colors.placeholder}]}
            numberOfLines={2}>
            {item.desc}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
      <StatusBarDC />
      <View style={{alignItems: 'center'}}>
        {shimmer === true ? (
          <View>
            <ShimmerPlaceHolder
              LinearGradient={LinearGradient}
              style={{
                marginHorizontal: 16,
                marginVertical: 4,
                borderRadius: 15,
                height: 256,
                width: screenWidth - 32,
              }}
            />
            <ShimmerPlaceHolder
              LinearGradient={LinearGradient}
              style={{
                marginHorizontal: 16,
                marginVertical: 4,
                borderRadius: 15,
                height: 256,
                width: screenWidth - 32,
              }}
            />
            <ShimmerPlaceHolder
              LinearGradient={LinearGradient}
              style={{
                marginHorizontal: 16,
                marginVertical: 4,
                borderRadius: 15,
                height: 256,
                width: screenWidth - 32,
              }}
            />
          </View>
        ) : (
          <FlatList
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={onRefresh}
                colors={[Colors.primary]}
              />
            }
            showsVerticalScrollIndicator={false}
            data={dataEvent}
            renderItem={renderEvent}
            showsHorizontalScrollIndicator={false}
            style={{
              paddingTop: 12,
              alignSelf: 'center',
            }}
            ListFooterComponent={<View style={{height: 24}} />}
          />
        )}
      </View>
    </SafeAreaView>
  );
};

export default index;

const styles = StyleSheet.create({});
