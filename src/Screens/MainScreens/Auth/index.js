import {StyleSheet, Text, View, SafeAreaView, Image} from 'react-native';
import React, {useState} from 'react';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import StatusBarDC from '../../Components/StatusBarDC';
import {TouchableOpacity} from 'react-native-gesture-handler';
import InputComponent from '../../Components/InputComponent';
import BtnPrimary from '../../Components/BtnPrimary';
import AntDesign from 'react-native-vector-icons/AntDesign';
// import * as Animatable from 'react-native-animatable';

const index = ({navigation}) => {
  const [role, setRole] = useState('User');
  const [form, setForm] = useState('Login');

  const renderRole = (title, image) => {
    return (
      <TouchableOpacity
        onPress={() => setRole(title)}
        style={{
          flex: 1,
          borderRadius: 100,
          backgroundColor: role === title ? Colors.primary : Colors.container,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          width: 120,
        }}>
        <Image
          source={image}
          style={{
            width: 24,
            height: 24,
            tintColor: role === title ? Colors.white : Colors.placeholder,
            marginRight: 10,
          }}
        />
        <Text
          style={[
            Font.reguler12,
            role === title
              ? {color: Colors.white}
              : {color: Colors.placeholder},
          ]}>
          {title}
        </Text>
      </TouchableOpacity>
    );
  };

  const renderForm = title => {
    return (
      <View style={{flex: 1}}>
        <TouchableOpacity
          style={{alignItems: 'center', paddingVertical: 8}}
          onPress={() => setForm(title)}>
          <Text
            style={[
              Font.reguler12,
              form === title
                ? {color: Colors.primary}
                : {color: Colors.placeholder},
            ]}>
            {title}
          </Text>
        </TouchableOpacity>
        <View
          style={
            form === title
              ? {
                  borderBottomColor: Colors.primary,
                  borderBottomWidth: 1,
                }
              : {
                  borderBottomColor: Colors.placeholder,
                  borderBottomWidth: 0.5,
                }
          }
        />
      </View>
    );
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: Colors.secondary}}>
      <StatusBarDC />
      <View style={{flex: 1}}>
        <View style={{height: 44}} />
        <View style={{alignItems: 'center'}}>
          <Image
            source={require('../../../Assets/images/logo2.png')}
            style={{width: 61, height: 95}}
            resizeMode={'contain'}
          />
          <View style={{height: 21}} />
          <Text style={[Font.bold24, {color: Colors.primary}]}>Welcome</Text>
        </View>
        <View style={{height: 12}} />
        {/* body */}
        <View style={styles.wrapBody}>
          <View style={styles.switchRole}>
            {renderRole(
              'User',
              require('../../../Assets/icons/User_circle.png'),
            )}
            {renderRole('Store', require('../../../Assets/icons/Shop.png'))}
          </View>
          <View style={{height: 16}} />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
            }}>
            {renderForm('Login')}
            {renderForm('Daftar')}
          </View>

          {/* form */}
          {form === 'Login' ? (
            <View style={{marginHorizontal: 16}}>
              <InputComponent
                title="Email"
                icon={require('../../../Assets/icons/Message_fill.png')}
              />
              <InputComponent
                title="Password"
                icon={require('../../../Assets/icons/Lock_fill.png')}
                secure={true}
              />
              <View style={{height: 16}} />
              <Text
                onPress={() => alert('Forgot Password')}
                style={[
                  Font.bold12,
                  {color: Colors.primary, textAlign: 'right'},
                ]}>
                Lupa Password?
              </Text>
              <View style={{height: 16}} />
              <BtnPrimary
                title={'Login'}
                pressed={() => navigation.navigate('Verifikasi')}
              />
            </View>
          ) : (
            <View style={{marginHorizontal: 16}}>
              <InputComponent
                title="Nama Lengkap"
                icon={require('../../../Assets/icons/User_fill.png')}
              />
              <InputComponent
                title="Email"
                icon={require('../../../Assets/icons/Message_fill.png')}
              />
              <InputComponent
                title="No. Telpon"
                icon={require('../../../Assets/icons/Phone_fill.png')}
              />
              <InputComponent
                title="Password"
                icon={require('../../../Assets/icons/Lock_fill.png')}
                secure={true}
              />
              <InputComponent
                title="Konfirmasi Password"
                icon={require('../../../Assets/icons/Lock_fill.png')}
                secure={true}
              />
              <View style={{height: 10}} />
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <AntDesign
                  name="checkcircle"
                  size={12}
                  color={Colors.success}
                  style={{marginRight: 8}}
                />
                <Text style={[Font.reguler12, {color: Colors.success}]}>
                  Password kamu sesuai
                </Text>
              </View>
              <View style={{height: 16}} />
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <AntDesign
                  name="checkcircle"
                  size={16}
                  color={Colors.primary}
                  style={{marginRight: 8}}
                />
                <Text style={[Font.reguler12, {flex: 1}]}>
                  Saya menyetujui{' '}
                  <Text style={[Font.bold12, {color: Colors.primary}]}>
                    Syarat Dan Ketentuan
                  </Text>{' '}
                  dan{' '}
                  <Text style={[Font.bold12, {color: Colors.primary}]}>
                    Kebijakan Privasi
                  </Text>
                  yang berlaku
                </Text>
              </View>
              <View style={{height: 16}} />
              <BtnPrimary
                title={'Daftar'}
                pressed={() => navigation.navigate('Verifikasi')}
              />
            </View>
          )}
        </View>
      </View>
    </SafeAreaView>
  );
};

export default index;

const styles = StyleSheet.create({
  wrapBody: {
    flex: 1,
    backgroundColor: Colors.white,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingVertical: 16,
  },
  switchRole: {
    backgroundColor: Colors.container,
    flexDirection: 'row',
    borderRadius: 100,
    height: 42,
    width: 247,
    alignSelf: 'center',
    padding: 4,
  },
});
