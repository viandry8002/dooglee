import Axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  RefreshControl,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';

const dataPembayaran = [
  {
    title: 'Transfer Virtual Account',
    items: [
      {
        id: 8,
        type: 'bank_transfer',
        code: 'bca',
        title: 'BCA Virtual Account',
        file: 'https://demoteknologi.com/assets/img/bca.png',
        fee_type: 'nominal',
        fee: 4000,
      },
      {
        id: 10,
        type: 'bank_transfer',
        code: 'bri',
        title: 'BNI Virtual Account',
        file: 'https://demoteknologi.com/assets/img/bri.png',
        fee_type: 'nominal',
        fee: 4000,
      },
      {
        id: 11,
        type: 'bank_transfer',
        code: 'bni',
        title: 'BRI Virtual Account',
        file: 'https://demoteknologi.com/assets/img/bni.png',
        fee_type: 'nominal',
        fee: 4000,
      },
      {
        id: 13,
        type: 'bank_transfer',
        code: 'echannel',
        title: 'Mandiri Bill Payment',
        file: 'https://demoteknologi.com/assets/img/mandiri.png',
        fee_type: 'nominal',
        fee: 4000,
      },
    ],
  },
  {
    title: 'Transfer Bank Manual (Gratis biaya, proses 1x24 jam)',
    items: [
      {
        id: 20,
        type: 'transfer_manual',
        code: '9384767893',
        title: 'Bank BCA',
        file: 'https://demoteknologi.com/assets/img/bca.png',
        fee_type: 'nominal',
        fee: 0,
      },
      {
        id: 21,
        type: 'transfer_manual',
        code: '45265789708',
        title: 'Bank Mandiri',
        file: 'https://demoteknologi.com/assets/img/mandiri.png',
        fee_type: 'nominal',
        fee: 0,
      },
      {
        id: 22,
        type: 'transfer_manual',
        code: '9384767893',
        title: 'Bank BRI',
        file: 'https://demoteknologi.com/assets/img/bri.png',
        fee_type: 'nominal',
        fee: 0,
      },
      {
        id: 23,
        type: 'transfer_manual',
        code: '45265789708',
        title: 'Bank BNI',
        file: 'https://demoteknologi.com/assets/img/bni.png',
        fee_type: 'nominal',
        fee: 0,
      },
    ],
  },
  {
    title: 'Gerai Retail',
    items: [
      {
        id: 17,
        type: 'cstore',
        code: 'indomaret',
        title: 'Indomaret',
        file: 'https://demoteknologi.com/assets/img/indomaret.png',
        fee_type: 'nominal',
        fee: 5000,
      },
      {
        id: 18,
        type: 'cstore',
        code: 'alfamart',
        title: 'Alfamart',
        file: 'https://demoteknologi.com/assets/img/alfamart.png',
        fee_type: 'nominal',
        fee: 5000,
      },
    ],
  },
];

const index = ({navigation, route}) => {
  const [refreshing, setRefreshing] = useState(false);
  //   const [data, setData] = useState([]);

  //   useEffect(() => {
  //     setRefreshing(true);
  //     setTimeout(() => {
  //       getData();
  //     }, 100);
  //   }, []);

  //   const onRefresh = () => {
  //     setRefreshing(true);
  //     setTimeout(() => {
  //       getData();
  //     }, 1000);
  //   };

  const renderItem = ({item}) => (
    <TouchableOpacity
      style={styles.wrapItem}
      onPress={() =>
        navigation.navigate('Checkout', {
          carts: route.params.carts,
          payment: item,
          voucher: route.params.voucher,
        })
      }>
      <Image
        source={{uri: item.file}}
        style={styles.imageItem}
        resizeMode={'contain'}
      />
      <Text style={{fontSize: 12, color: Colors.black}}>{item.title}</Text>
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: Colors.white, padding: 8}}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        // refreshControl={
        //   <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        // }
      >
        {dataPembayaran.map(dt => (
          <FlatList
            data={dt.items}
            renderItem={renderItem}
            ListHeaderComponent={
              <View style={styles.headerItem}>
                <Text style={[Font.bold12, {color: Colors.primary}]}>
                  {dt.title}
                </Text>
              </View>
            }
            showsVerticalScrollIndicator={false}
          />
        ))}
      </ScrollView>
    </SafeAreaView>
  );
};

export default index;

const styles = StyleSheet.create({
  headerItem: {
    padding: 8,
    backgroundColor: Colors.lightPrimary,
    borderRadius: 7,
  },

  wrapItem: {padding: 8, flexDirection: 'row', marginVertical: 8},
  imageItem: {width: 48, height: 16, marginRight: 32},
});
