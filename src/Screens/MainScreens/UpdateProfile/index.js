import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Image,
  ImageBackground,
  TouchableOpacity,
  FlatList,
  ScrollView,
} from 'react-native';
import React from 'react';
import {screenWidth} from '../../../Variable';
import Font from '../../../Styles/Fonts';
import Colors from '../../../Styles/Colors';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import InputNormal from '../../Components/InputNormal';
import BtnPrimary from '../../Components/BtnPrimary';

const dataChild = [
  {
    id: 1,
    image: require('../../../Assets/images/boy.png'),
    title: 'ren',
    usia: 'Usia 7 Minggu 2 Hari',
  },
  {
    id: 2,
    image: require('../../../Assets/images/girl.png'),
    title: 'kayla',
    usia: 'Lahir 8 April 2022',
  },
];

const index = ({navigation}) => {
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
      <Image
        source={require('../../../Assets/images/bgProfile.png')}
        style={{
          height: 218,
          width: screenWidth,
          position: 'absolute',
        }}
      />

      <ScrollView showsVerticalScrollIndicator={false}>
        <View
          style={{
            alignItems: 'center',
            height: 180,
          }}>
          <View style={{height: 60}} />
          <Image
            source={require('../../../Assets/images/dokter.png')}
            style={{width: 73, height: 73}}
          />
          <View style={{height: 8}} />
          <Text
            style={[Font.reguler12, {color: Colors.white}]}
            onPress={() => {}}>
            Upload Gambar
          </Text>
          <View style={{height: 8}} />
        </View>
        {/* <ScrollView style={{backgroundColor: 'red'}}> */}
        {/* body */}
        <View style={{flex: 1, alignItems: 'center'}}>
          {/* elevation1 */}
          <View style={styles.wrapElevation}>
            <View style={styles.TitleTwo}>
              <Text style={Font.bold16}>Profile Saya</Text>
            </View>
            <InputNormal
              val={'Riska Sulistiawati'}
              label={'Nama Lengkap'}
              title={''}
            />
            <InputNormal
              val={'riskasulistiawati@gmail.com'}
              label={'Alamat Email'}
              title={''}
            />
            <InputNormal val={'089123249284'} label={'No. Telpon'} title={''} />
            <View style={{height: 16}} />
            <TouchableOpacity
              style={{
                backgroundColor: Colors.lightPrimary,
                height: 48,
                alignItems: 'center',
                justifyContent: 'center',
                borderRadius: 100,
                borderWidth: 1,
                borderColor: Colors.primary,
                flexDirection: 'row',
              }}
              onPress={() => {}}>
              <Image
                source={require('../../../Assets/icons/Lock_fill.png')}
                style={{width: 24, height: 24}}
              />
              <Text
                style={[
                  Font.reguler12,
                  {color: Colors.primary, marginLeft: 16},
                ]}>
                Ubah Password
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
      <View style={styles.absoluteFooter1}>
        <View style={{flex: 1}}>
          <BtnPrimary
            title={'Simpan'}
            pressed={() => navigation.navigate('Home')}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

export default index;

const styles = StyleSheet.create({
  TitleTwo: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  wrapElevation: {
    width: screenWidth - 16,
    backgroundColor: Colors.white,
    borderRadius: 15,
    elevation: 10,
    // top: 210,
    padding: 16,
    marginBottom: 16,
  },
  absoluteFooter1: {
    flexDirection: 'row',
    backgroundColor: Colors.white,
    padding: 16,
    elevation: 10,
  },
});
