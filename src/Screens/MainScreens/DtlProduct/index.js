import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  ScrollView,
  Image,
} from 'react-native';
import React, {useState} from 'react';
import {SliderBox} from 'react-native-image-slider-box';
import {screenWidth} from '../../../Variable';
import Colors from '../../../Styles/Colors';
import Money from '../../Components/Money';
import Font from '../../../Styles/Fonts';

const data2 = [
  require('../../../Assets/images/product1.png'),
  require('../../../Assets/images/product2.png'),
  require('../../../Assets/images/product1.png'),
];

const datafilter = [
  {
    id: 1,
    title: 'Candy Pink',
  },
  {
    id: 2,
    title: 'Aqua Blue',
  },
  {
    id: 3,
    title: 'Golden Beige',
  },
  {
    id: 4,
    title: 'Seal Grey',
  },
  {
    id: 5,
    title: 'Candy Pink',
  },
  {
    id: 6,
    title: 'Aqua Blue',
  },
  {
    id: 7,
    title: 'Golden Beige',
  },
  {
    id: 8,
    title: 'Seal Grey',
  },
];

const index = () => {
  const [chose, setChose] = useState(1);

  const renderFilter = ({item}) => {
    return (
      <TouchableOpacity
        style={
          chose === item.id
            ? {
                flex: 1,
                height: 32,
                backgroundColor: Colors.lightPrimary,
                paddingHorizontal: 16,
                alignItems: 'center',
                justifyContent: 'center',
                marginRight: 8,
                borderRadius: 100,
              }
            : {
                flex: 1,
                height: 32,
                backgroundColor: Colors.white,
                paddingHorizontal: 16,
                alignItems: 'center',
                justifyContent: 'center',
                marginRight: 8,
                borderRadius: 100,
                borderWidth: 1,
                borderColor: Colors.container,
              }
        }
        onPress={() => setChose(item.id)}>
        {chose === item.id ? (
          <Text style={[Font.bold12, {color: Colors.primary}]}>
            {item.title}
          </Text>
        ) : (
          <Text style={[Font.reguler12, {color: Colors.gray1}]}>
            {item.title}
          </Text>
        )}
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      {/* bannerTop */}
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={{alignItems: 'center'}}>
          <View style={{paddingHorizontal: 16, height: 375}}>
            <SliderBox
              autoplay
              circleLoop
              images={data2}
              ImageComponentStyle={{
                height: 375,
                width: screenWidth,
              }}
              dotStyle={{
                width: 8,
                height: 8,
                marginHorizontal: -22,
              }}
              dotColor={Colors.black}
              inactiveDotColor={Colors.container}
              // parentWidth={screenWidth - 32}
            />
          </View>
        </View>
        {/* title and price */}
        <View style={{flex: 1, padding: 16, backgroundColor: Colors.white}}>
          <Text style={Font.reguler16}>Dooglee - Newborn Pillow Dooglee</Text>
          <View style={{height: 4}} />
          <Money
            value={599000}
            styling={[Font.bold16, {color: Colors.primary}]}
          />
          <View style={{height: 4}} />
          <Money
            value={700000}
            styling={[
              Font.reguler12,
              {
                color: Colors.placeholder,
                textDecorationStyle: 'solid',
                textDecorationLine: 'line-through',
              },
            ]}
          />
        </View>
        <View style={{height: 8}} />
        {/* type */}
        <View
          style={{
            flex: 1,
            backgroundColor: Colors.white,
            paddingVertical: 16,
          }}>
          <Text style={[Font.reguler14, {marginHorizontal: 16}]}>
            Pilih Warna
          </Text>
          <View style={{height: 16}} />
          <FlatList
            data={datafilter}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            renderItem={renderFilter}
            ListHeaderComponent={<View style={{width: 16}} />}
          />
        </View>
        <View style={{height: 8}} />
        {/* desc */}
        <View
          style={{
            flex: 1,
            backgroundColor: Colors.white,
            padding: 16,
          }}>
          <Text style={Font.reguler14}>Deskripsi Produk</Text>
          <View style={{height: 4}} />
          <Text style={[Font.reguler12, {color: Colors.placeholder}]}>
            Dooglee Newborn Pillow adalah bantal berbahan 100% Natural Latex
            yang dirancang khusus untuk mengurangi tekanan pada bagian kepala
            bayi saat tidur sehingga dapat membentuk dan menjaga kepala agar
            tetap simetris. Berdasarkan bentuk dan ukuran, Dooglee Newborn
            Pillow sangat cocok untuk bayi yang baru lahir karena dapat
            mencegahan dan mengobati plagiocephaly (sindrom kepala datar). Untuk
            mendapatkan fungsi maksimal, penggunan Dooglee Newborn Pillow
            dianjurkan sejak bayi baru lahir hingga umur 10 bulan. Dengan
            kandungan benang sutera pada kain knitting, Dooglee Newborn Pillow
            menambah kualitas kenyamanan tidur bayi karena bahan Sutra memiliki
            tekstur yang sangat lembut, sejuk, lentur, serta memiliki daya serap
            yang baik. Sarung bantal opsional tersedia untuk dibeli secara
            terpisah. Design yang ergonomis untuk mempermudah pemasangan,
            pelepasan dan pencucian. Untuk pembelian Newborn pillow sudah Free
            case 1pcs - White Pearl Dimensi Produk: +/-30 x 23 x 3cm Dimensi
            Dus: 31 x 23 x 4cm
          </Text>
          <View style={{height: 16}} />
        </View>
      </ScrollView>
      <View style={styles.absoluteFooter1}>
        <View style={{flex: 1}}>
          <TouchableOpacity
            style={{
              backgroundColor: Colors.primary,
              height: 48,
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: 100,
              flexDirection: 'row',
            }}
            onPress={() => {}}>
            <Image
              source={require('../../../Assets/icons/Bag_fill.png')}
              style={{width: 18, height: 18, marginRight: 8}}
            />
            <Text style={[Font.reguler14, {color: Colors.white}]}>
              Tambah Keranjang
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default index;

const styles = StyleSheet.create({
  absoluteFooter1: {
    flexDirection: 'row',
    backgroundColor: Colors.white,
    padding: 16,
    elevation: 10,
  },
});
