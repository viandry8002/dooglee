import {
  FlatList,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  RefreshControl,
  TouchableOpacity,
  Image,
} from 'react-native';
import React, {useState} from 'react';
import Colors from '../../../Styles/Colors';
import {screenWidth} from '../../../Variable';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Font from '../../../Styles/Fonts';
import BtnPrimary from '../../Components/BtnPrimary';

const dataAdress = [
  {
    id: 1,
    name: 'Alvian',
    tlp: '08122334458',
    address:
      'Jl. Petojo Vij III No.17-77, RT.12/RW.6, Cideng, Kecamatan Gambir, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10150',
  },
  {
    id: 2,
    name: 'Alvian',
    tlp: '08122334458',
    address:
      'Jl. Petojo Vij III No.17-77, RT.12/RW.6, Cideng, Kecamatan Gambir, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10150',
  },
  {
    id: 3,
    name: 'Alvian',
    tlp: '08122334458',
    address:
      'Jl. Petojo Vij III No.17-77, RT.12/RW.6, Cideng, Kecamatan Gambir, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10150',
  },
];

const index = ({navigation}) => {
  const [refreshing, setRefreshing] = useState(false);
  const [chose, setChose] = useState(0);
  const [shimmer, setShimmer] = useState(true);

  //   useEffect(() => {
  //     setTimeout(() => {
  //       setShimmer(false);
  //     }, 2000);
  //   }, []);

  const onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  };

  const renderEvent = ({item}) => {
    return (
      <View
        style={
          item.id === chose
            ? {
                marginHorizontal: 16,
                marginVertical: 4,
                borderRadius: 15,
                height: 162,
                width: screenWidth - 32,
                backgroundColor: Colors.lightPrimary,
                elevation: 4,
                flex: 0.5,
                padding: 16,
              }
            : {
                marginHorizontal: 16,
                marginVertical: 4,
                borderRadius: 15,
                height: 162,
                width: screenWidth - 32,
                backgroundColor: Colors.white,
                elevation: 4,
                flex: 0.5,
                padding: 16,
              }
        }>
        <TouchableOpacity onPress={() => setChose(item.id)}>
          <Text style={Font.bold12}>{item.name}</Text>
          <Text style={Font.reguler12}>{item.tlp}</Text>
          <Text style={Font.reguler12} numberOfLines={3}>
            {item.address}
          </Text>
        </TouchableOpacity>
        <View style={{height: 16}} />
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TouchableOpacity
              style={{flexDirection: 'row', alignItems: 'center'}}>
              <MaterialCommunityIcons
                name="pencil"
                size={13}
                color={Colors.primary}
              />
              <Text
                style={[
                  Font.reguler12,
                  {color: Colors.primary, marginLeft: 8},
                ]}>
                Edit Alamat
              </Text>
            </TouchableOpacity>
            <View style={{width: 16}} />
            <TouchableOpacity
              style={{flexDirection: 'row', alignItems: 'center'}}>
              <Image
                source={require('../../../Assets/icons/Trash.png')}
                style={{width: 18, height: 18}}
              />
              <Text
                style={[
                  Font.reguler12,
                  {color: Colors.primary, marginLeft: 8},
                ]}>
                Hapus Alamat
              </Text>
            </TouchableOpacity>
          </View>
          {item.id === chose ? (
            <AntDesign name="checkcircle" size={18} color={Colors.primary} />
          ) : (
            false
          )}
        </View>
      </View>
    );
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
      <View style={{alignItems: 'center', flex: 1}}>
        <FlatList
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={onRefresh}
              colors={[Colors.primary]}
            />
          }
          showsVerticalScrollIndicator={false}
          data={dataAdress}
          renderItem={renderEvent}
          showsHorizontalScrollIndicator={false}
          style={{
            paddingTop: 12,
            alignSelf: 'center',
          }}
          ListFooterComponent={
            <TouchableOpacity
              style={{
                backgroundColor: Colors.lightPrimary,
                height: 48,
                alignItems: 'center',
                justifyContent: 'center',
                borderRadius: 100,
                marginHorizontal: 16,
                flexDirection: 'row',
                marginTop: 8,
              }}
              onPress={() => navigation.navigate('FormAlamat')}>
              <AntDesign name="plus" size={18} color={Colors.primary} />
              <Text
                style={[
                  Font.reguler12,
                  {color: Colors.primary, marginLeft: 16},
                ]}>
                Tambah Alamat
              </Text>
            </TouchableOpacity>
          }
        />
      </View>
      <View style={styles.absoluteFooter1}>
        <View style={{flex: 1}}>
          <BtnPrimary
            title={'Konfirmasi'}
            pressed={() => navigation.navigate('Home')}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

export default index;

const styles = StyleSheet.create({
  absoluteFooter1: {
    flexDirection: 'row',
    backgroundColor: Colors.white,
    padding: 16,
    elevation: 10,
  },
});
