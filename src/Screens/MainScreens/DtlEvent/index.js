import {SafeAreaView, StyleSheet, Text, View, Image} from 'react-native';
import React from 'react';
import Colors from '../../../Styles/Colors';
import {screenWidth} from '../../../Variable';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Font from '../../../Styles/Fonts';
import BtnPrimary from '../../Components/BtnPrimary';

const index = ({navigation}) => {
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
      <Image
        source={require('../../../Assets/images/banner.png')}
        style={{width: screenWidth, height: 174}}
        resizeMode={'cover'}
      />
      <View style={{height: 16}} />
      <View style={{flex: 1, marginHorizontal: 16}}>
        <Text style={Font.bold16}>Healty Mom’s Yoga Prenatal Class</Text>
        <View style={{height: 8}} />
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <AntDesign name="clockcircleo" size={16} color={Colors.placeholder} />
          <Text
            style={[
              Font.reguler12,
              {color: Colors.placeholder, marginLeft: 8},
            ]}>
            23 April 2022, 08:00 - 17:00
          </Text>
        </View>
        <View style={{height: 8}} />
        <Text style={[Font.reguler12, {color: Colors.placeholder}]}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Est quisque
          cursus vulputate aliquam eu proin amet sed. Massa, venenatis felis
          turpis id sit consectetur. Neque sit sit nisl proin eu amet, lectus
          enim ante. Dui, fames at nec mi augue urna a. Imperdiet ultricies diam
          neque, sodales vulputate elementum maecenas porta. Risus euismod et
          tellus nibh in id lobortis at molestie. Pharetra cursus a, neque
          ultrices feugiat rutrum imperdiet. Placerat eu dui aliquam ultricies
          molestie pretium, risus. Hendrerit dictum aliquet platea pellentesque
          curabitur condimentum viverra. Et, volutpat morbi elementum aliquam
          tempor cras mauris urna. Convallis vestibulum, ac egestas ipsum. Eget
          habitant orci, egestas mattis viverra. Scelerisque.
        </Text>
      </View>
      <View style={styles.absoluteFooter}>
        <BtnPrimary title={'Gabung Sekarang'} pressed={() => {}} />
      </View>
    </SafeAreaView>
  );
};

export default index;

const styles = StyleSheet.create({
  absoluteFooter: {
    backgroundColor: Colors.white,
    padding: 16,
    elevation: 10,
  },
});
