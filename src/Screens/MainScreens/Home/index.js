import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  StatusBar,
  FlatList,
  Image,
  TouchableOpacity,
  ScrollView,
  RefreshControl,
  ImageBackground,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import Colors from '../../../Styles/Colors';
import {SliderBox} from 'react-native-image-slider-box';
import {screenWidth} from '../../../Variable';
import Font from '../../../Styles/Fonts';
import AntDesign from 'react-native-vector-icons/AntDesign';
// import {TextMask} from 'react-native-masked-text';
import Money from '../../Components/Money';
import Trimester from '../../Components/Trimester';
import ShimmerPlaceHolder from 'react-native-shimmer-placeholder';
import LinearGradient from 'react-native-linear-gradient';

const dataBayi = [
  {
    id: 1,
    image: require('../../../Assets/images/anak1.png'),
  },
  {
    id: 2,
    image: require('../../../Assets/images/anak1.png'),
  },
  {
    id: 3,
    image: require('../../../Assets/images/anak1.png'),
  },
];

const data2 = [
  require('../../../Assets/images/bannerTop.png'),
  require('../../../Assets/images/bannerTop.png'),
  require('../../../Assets/images/bannerTop.png'),
];

const dataProduct = [
  {
    id: 1,
    image: require('../../../Assets/images/product1.png'),
    title: 'Dooglee - Newborn Pillow Dooglee',
    price: 599000,
    disc: 700000,
  },
  {
    id: 2,
    image: require('../../../Assets/images/product2.png'),
    title: 'Dooglee - Newborn Pillow Dooglee',
    price: 599000,
    disc: 700000,
  },
  {
    id: 3,
    image: require('../../../Assets/images/product2.png'),
    title: 'Dooglee - Newborn Pillow Dooglee',
    price: 599000,
    disc: 700000,
  },
];

const dataEvent = [
  {
    id: 1,
    image: require('../../../Assets/images/banner.png'),
    title: 'Healty Mom’s Yoga Prenatal Class',
    date: '23 April 2022, 08:00 - 17:00',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Senectus nunc nec nunc mollis.......',
  },
  {
    id: 2,
    image: require('../../../Assets/images/banner.png'),
    title: 'Healty Mom’s Yoga Prenatal Class',
    date: '23 April 2022, 08:00 - 17:00',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Senectus nunc nec nunc mollis.......',
  },
  {
    id: 3,
    image: require('../../../Assets/images/banner.png'),
    title: 'Healty Mom’s Yoga Prenatal Class',
    date: '23 April 2022, 08:00 - 17:00',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Senectus nunc nec nunc mollis.......',
  },
];

const dataDokter = [
  {
    id: 1,
    image: require('../../../Assets/images/dokter.png'),
    nama: 'Dr. Michael',
    desc: '“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pretium sit fusce feugiat non sed fames. Sit.”',
  },
  {
    id: 2,
    image: require('../../../Assets/images/dokter.png'),
    nama: 'Dr. Michael',
    desc: '“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pretium sit fusce feugiat non sed fames. Sit.”',
  },
  {
    id: 3,
    image: require('../../../Assets/images/dokter.png'),
    nama: 'Dr. Michael',
    desc: '“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pretium sit fusce feugiat non sed fames. Sit.”',
  },
];

const dataKomunitas = [
  {
    id: 1,
    image: require('../../../Assets/images/slider1.png'),
  },
  {
    id: 2,
    image: require('../../../Assets/images/slider1.png'),
  },
  {
    id: 3,
    image: require('../../../Assets/images/slider1.png'),
  },
];

const index = ({navigation}) => {
  const [refreshing, setRefreshing] = useState(false);
  const [shimmer, setShimmer] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setShimmer(false);
    }, 2000);
  }, []);

  const onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  };

  const renderItem = ({item}) => {
    return (
      // <View style={{marginRight: 16, borderRadius: 15}}>
      //   <Image source={item.image} style={{width: 281, height: 135}} />
      // </View>
      <TouchableOpacity
        style={{marginRight: 8, borderRadius: 15}}
        onPress={() => navigation.navigate('Pregnancy')}>
        <ImageBackground
          source={require('../../../Assets/images/bgBaby.png')}
          style={{
            width: 281,
            height: 135,
            flexDirection: 'row',
            padding: 8,
            alignItems: 'center',
          }}>
          <View style={{margin: 8, flex: 1}}>
            <Text style={Font.bold16}>My Baby</Text>
            <Text style={Font.reguler12}>7 Minggu 3 Hari</Text>
            <Trimester width={52} showTrimester={false} />
            <Text style={[Font.reguler12, {color: Colors.placeholder}]}>
              Trimester 1
            </Text>
            <Text style={[Font.bold12, {color: Colors.primary}]}>
              Perkiraan Lahir : 12 April 2023
            </Text>
          </View>
          <Image
            source={require('../../../Assets/images/janin.png')}
            style={{width: 90, height: 90}}
          />
        </ImageBackground>
      </TouchableOpacity>
    );
  };

  const renderTitleTwo = (title, pressed) => {
    return (
      <View style={styles.TitleTwo}>
        <Text style={Font.bold14}>{title}</Text>
        <Text style={[Font.bold12, {color: Colors.primary}]} onPress={pressed}>
          Lihat Semua{' '}
          <AntDesign name="right" size={10} color={Colors.primary} />
        </Text>
      </View>
    );
  };

  const renderTitleOne = title => {
    return (
      <View style={styles.TitleTwo}>
        <Text style={Font.bold14}>{title}</Text>
      </View>
    );
  };

  const renderProduct = ({item}) => {
    return (
      <TouchableOpacity
        style={{
          marginHorizontal: 4,
          borderRadius: 15,
          height: 256,
          width: 167,
          backgroundColor: Colors.white,
          elevation: 4,
          flex: 0.5,
        }}
        onPress={() => navigation.navigate('DtlProduct')}>
        <Image
          source={item.image}
          style={{
            width: '100%',
            height: 167,
            borderTopLeftRadius: 15,
            borderTopRightRadius: 15,
            resizeMode: 'cover',
          }}
        />
        <View style={{flex: 1, margin: 8, justifyContent: 'space-between'}}>
          <Text style={Font.reguler12}>{item.title}</Text>
          <Money
            value={item.price}
            styling={[Font.bold14, {color: Colors.primary}]}
          />
          <Money
            value={item.disc}
            styling={[
              Font.reguler12,
              {
                color: Colors.placeholder,
                textDecorationStyle: 'solid',
                textDecorationLine: 'line-through',
              },
            ]}
          />
        </View>
      </TouchableOpacity>
    );
  };

  const renderEvent = ({item}) => {
    return (
      <TouchableOpacity
        style={{
          marginHorizontal: 4,
          borderRadius: 15,
          height: 256,
          width: 343,
          backgroundColor: Colors.white,
          elevation: 4,
          flex: 0.5,
        }}>
        <Image
          source={item.image}
          style={{
            width: 343,
            height: 159,
            borderTopLeftRadius: 15,
            borderTopRightRadius: 15,
            resizeMode: 'cover',
          }}
        />
        <View style={{flex: 1, margin: 8, justifyContent: 'space-between'}}>
          <Text style={Font.bold14}>{item.title}</Text>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <AntDesign
              name="clockcircleo"
              size={16}
              color={Colors.placeholder}
            />
            <Text
              style={[
                Font.reguler12,
                {color: Colors.placeholder, marginLeft: 8},
              ]}>
              {item.date}
            </Text>
          </View>
          <Text
            style={[Font.reguler12, {color: Colors.placeholder}]}
            numberOfLines={2}>
            {item.desc}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  const renderDokter = ({item}) => {
    return (
      <TouchableOpacity
        style={{
          marginHorizontal: 4,
          borderRadius: 15,
          height: 104,
          width: 343,
          backgroundColor: Colors.lightPrimary,
          elevation: 4,
          flex: 0.5,
          padding: 16,
          flexDirection: 'row',
        }}>
        <Image
          source={item.image}
          style={{
            width: 72,
            height: 72,
            borderRadius: 100,
            resizeMode: 'cover',
          }}
        />
        <View style={{flex: 1, marginLeft: 24}}>
          <Text
            style={[Font.reguler12, {color: Colors.gray1}]}
            numberOfLines={3}>
            {item.desc}
          </Text>
          <View style={{height: 8}} />
          <Text style={[Font.bold12, {color: Colors.gray1}]} numberOfLines={1}>
            {item.nama}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  const renderKomunitas = ({item}) => {
    return (
      <TouchableOpacity
        style={{
          marginHorizontal: 4,
          borderRadius: 15,
          height: 160,
          width: 343,
          elevation: 4,
          flex: 0.5,
        }}>
        <Image
          source={item.image}
          style={{
            width: 343,
            height: 160,
            borderRadius: 15,
            resizeMode: 'cover',
          }}
        />
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
      <StatusBar
        backgroundColor="transparent"
        translucent
        barStyle="light-content"
      />
      {/* body */}
      {shimmer === true ? (
        <ScrollView
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={onRefresh}
              colors={[Colors.primary]}
            />
          }>
          <View style={{height: 70, backgroundColor: Colors.primary}} />

          <View
            style={{
              position: 'absolute',
              marginLeft: 16,
              flexDirection: 'row',
            }}>
            <ShimmerPlaceHolder
              LinearGradient={LinearGradient}
              style={{
                marginRight: 8,
                width: 281,
                height: 135,
                borderRadius: 15,
              }}
            />

            <ShimmerPlaceHolder
              LinearGradient={LinearGradient}
              style={{
                marginRight: 8,
                width: 281,
                height: 135,
                borderRadius: 15,
              }}
            />
          </View>

          <View style={{height: 90}} />
          {/* bannerTop */}
          <View style={{alignItems: 'center'}}>
            <View style={{paddingHorizontal: 16, height: 185}}>
              <ShimmerPlaceHolder
                LinearGradient={LinearGradient}
                style={{
                  borderRadius: 15,
                  height: 157,
                  width: screenWidth - 32,
                }}
              />
            </View>
          </View>
          {renderTitleOne('Khusus untuk kamu & Si Kecil')}

          <View style={{height: 16}} />

          <View style={{paddingLeft: 12, height: 270, flexDirection: 'row'}}>
            <ShimmerPlaceHolder
              LinearGradient={LinearGradient}
              style={{
                marginHorizontal: 4,
                borderRadius: 15,
                height: 256,
                width: 167,
              }}
            />

            <ShimmerPlaceHolder
              LinearGradient={LinearGradient}
              style={{
                marginHorizontal: 4,
                borderRadius: 15,
                height: 256,
                width: 167,
              }}
            />

            <ShimmerPlaceHolder
              LinearGradient={LinearGradient}
              style={{
                marginHorizontal: 4,
                borderRadius: 15,
                height: 256,
                width: 167,
              }}
            />
          </View>
          <View style={{height: 16}} />
          {renderTitleOne('Event yang akan berlangsung')}
          <View style={{height: 16}} />
          <View style={{paddingLeft: 12, height: 270, flexDirection: 'row'}}>
            <ShimmerPlaceHolder
              LinearGradient={LinearGradient}
              style={{
                marginHorizontal: 4,
                borderRadius: 15,
                height: 256,
                width: 343,
              }}
            />

            <ShimmerPlaceHolder
              LinearGradient={LinearGradient}
              style={{
                marginHorizontal: 4,
                borderRadius: 15,
                height: 256,
                width: 343,
              }}
            />

            <ShimmerPlaceHolder
              LinearGradient={LinearGradient}
              style={{
                marginHorizontal: 4,
                borderRadius: 15,
                height: 256,
                width: 343,
              }}
            />
          </View>
          <View style={{height: 16}} />
          {renderTitleOne('Catatan Dokter')}
          <View style={{height: 16}} />
          <View style={{paddingLeft: 12, height: 140, flexDirection: 'row'}}>
            <ShimmerPlaceHolder
              LinearGradient={LinearGradient}
              style={{
                marginHorizontal: 4,
                borderRadius: 15,
                height: 104,
                width: 343,
              }}
            />

            <ShimmerPlaceHolder
              LinearGradient={LinearGradient}
              style={{
                marginHorizontal: 4,
                borderRadius: 15,
                height: 104,
                width: 343,
              }}
            />
          </View>
          {renderTitleOne('Komunitas Aktifasi')}
          <View style={{height: 16}} />
          <View style={{paddingLeft: 12, height: 200, flexDirection: 'row'}}>
            <ShimmerPlaceHolder
              LinearGradient={LinearGradient}
              style={{
                marginHorizontal: 4,
                borderRadius: 15,
                height: 160,
                width: 343,
              }}
            />

            <ShimmerPlaceHolder
              LinearGradient={LinearGradient}
              style={{
                marginHorizontal: 4,
                borderRadius: 15,
                height: 160,
                width: 343,
              }}
            />
          </View>
        </ScrollView>
      ) : (
        <ScrollView
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={onRefresh}
              colors={[Colors.primary]}
            />
          }>
          <View style={{height: 70, backgroundColor: Colors.primary}} />

          <FlatList
            data={dataBayi}
            renderItem={renderItem}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            style={{position: 'absolute', paddingLeft: 16}}
            ListFooterComponent={<View style={{width: 16}} />}
          />

          <View style={{height: 90}} />
          {/* bannerTop */}
          <View style={{alignItems: 'center'}}>
            <View style={{paddingHorizontal: 16, height: 185}}>
              <SliderBox
                autoplay
                circleLoop
                images={data2}
                ImageComponentStyle={{
                  borderRadius: 15,
                  height: 157,
                  width: screenWidth - 32,
                }}
                paginationBoxStyle={{
                  alignSelf: 'flex-start',
                }}
                dotStyle={{
                  width: 8,
                  height: 8,
                  marginHorizontal: -20,
                  left: -20,
                }}
                dotColor={Colors.primary}
                inactiveDotColor={Colors.container}
                parentWidth={screenWidth - 32}
              />
            </View>
          </View>
          <View style={{height: 16}} />
          {renderTitleTwo('Khusus untuk kamu & Si Kecil', () => {})}

          <View style={{height: 16}} />
          <FlatList
            data={dataProduct}
            renderItem={renderProduct}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            style={{
              paddingLeft: 12,
              height: 270,
            }}
            ListFooterComponent={<View style={{width: 24}} />}
          />
          <View style={{height: 16}} />
          {renderTitleTwo('Event yang akan berlangsung', () => {})}
          <View style={{height: 16}} />
          <FlatList
            data={dataEvent}
            renderItem={renderEvent}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            style={{
              paddingLeft: 12,
              height: 270,
            }}
            ListFooterComponent={<View style={{width: 24}} />}
          />
          <View style={{height: 16}} />
          {renderTitleOne('Catatan Dokter')}
          <View style={{height: 16}} />
          <FlatList
            data={dataDokter}
            renderItem={renderDokter}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            style={{
              paddingLeft: 12,
              height: 140,
            }}
            ListFooterComponent={<View style={{width: 24}} />}
          />
          {renderTitleOne('Komunitas Aktifasi')}
          <View style={{height: 16}} />
          <FlatList
            data={dataKomunitas}
            renderItem={renderKomunitas}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            style={{
              paddingLeft: 12,
              height: 200,
            }}
            ListFooterComponent={<View style={{width: 24}} />}
          />
        </ScrollView>
      )}
    </SafeAreaView>
  );
};

export default index;

const styles = StyleSheet.create({
  TitleTwo: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 16,
  },
});
