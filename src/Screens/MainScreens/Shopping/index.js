import {
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  FlatList,
  Image,
} from 'react-native';
import React, {useState} from 'react';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import Money from '../../Components/Money';
import StatusBarDC from '../../Components/StatusBarDC';

const datafilter = [
  {
    id: 1,
    title: 'Recommendation For You',
  },
  {
    id: 2,
    title: 'All Product',
  },
  {
    id: 3,
    title: 'Bedding',
  },
  {
    id: 4,
    title: 'Mattres',
  },
  {
    id: 5,
    title: 'Bathing',
  },
  {
    id: 6,
    title: 'Car seats & carriers',
  },
  {
    id: 7,
    title: 'clothing',
  },
];

const dataProduct = [
  {
    id: 1,
    image: require('../../../Assets/images/product1.png'),
    title: 'Dooglee - Newborn Pillow Dooglee',
    price: 599000,
    disc: 700000,
  },
  {
    id: 2,
    image: require('../../../Assets/images/product2.png'),
    title: 'Dooglee - Newborn Pillow Dooglee',
    price: 599000,
    disc: 700000,
  },
  {
    id: 3,
    image: require('../../../Assets/images/product2.png'),
    title: 'Dooglee - Newborn Pillow Dooglee',
    price: 599000,
    disc: 700000,
  },
  {
    id: 4,
    image: require('../../../Assets/images/product1.png'),
    title: 'Dooglee - Newborn Pillow Dooglee',
    price: 599000,
    disc: 700000,
  },
  {
    id: 5,
    image: require('../../../Assets/images/product2.png'),
    title: 'Dooglee - Newborn Pillow Dooglee',
    price: 599000,
    disc: 700000,
  },
  {
    id: 6,
    image: require('../../../Assets/images/product2.png'),
    title: 'Dooglee - Newborn Pillow Dooglee',
    price: 599000,
    disc: 700000,
  },
  {
    id: 7,
    image: require('../../../Assets/images/product2.png'),
    title: 'Dooglee - Newborn Pillow Dooglee',
    price: 599000,
    disc: 700000,
  },
];

const index = ({navigation}) => {
  const [chose, setChose] = useState(1);

  const renderFilter = ({item}) => {
    return (
      <View style={{flex: 1}}>
        <TouchableOpacity
          style={{
            flex: 1,
            backgroundColor: Colors.white,
            paddingHorizontal: 16,
            alignItems: 'center',
            justifyContent: 'center',
          }}
          onPress={() => setChose(item.id)}>
          {chose === item.id ? (
            <Text style={[Font.bold12, {color: Colors.primary}]}>
              {item.title}
            </Text>
          ) : (
            <Text style={[Font.reguler12, {color: Colors.placeholder}]}>
              {item.title}
            </Text>
          )}
        </TouchableOpacity>
        <View
          style={
            chose === item.id
              ? {
                  borderBottomColor: Colors.primary,
                  borderBottomWidth: 1,
                }
              : false
          }
        />
      </View>
    );
  };

  const renderProduct = ({item}) => {
    return (
      <TouchableOpacity
        style={{
          margin: 4,
          borderRadius: 15,
          height: 256,
          backgroundColor: Colors.white,
          elevation: 1,
          flex: 0.5,
        }}
        onPress={() => navigation.navigate('DtlProduct')}>
        <Image
          source={item.image}
          style={{
            width: '100%',
            height: 167,
            borderTopLeftRadius: 15,
            borderTopRightRadius: 15,
            resizeMode: 'cover',
          }}
        />
        <View style={{flex: 1, margin: 8, justifyContent: 'space-between'}}>
          <Text style={Font.reguler12}>{item.title}</Text>
          <Money
            value={item.price}
            styling={[Font.bold14, {color: Colors.primary}]}
          />
          <Money
            value={item.disc}
            styling={[
              Font.reguler12,
              {
                color: Colors.placeholder,
                textDecorationStyle: 'solid',
                textDecorationLine: 'line-through',
              },
            ]}
          />
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <StatusBarDC />
      <View style={{height: 40}}>
        <FlatList
          data={datafilter}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          renderItem={renderFilter}
        />
      </View>
      <FlatList
        showsVerticalScrollIndicator={false}
        data={dataProduct}
        renderItem={renderProduct}
        numColumns={2}
        style={{margin: 4}}
        ListFooterComponent={<View style={{height: 24}} />}
      />
    </SafeAreaView>
  );
};

export default index;

const styles = StyleSheet.create({});
