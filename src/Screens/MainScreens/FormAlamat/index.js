import {
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
  Image,
} from 'react-native';
import React from 'react';
import Colors from '../../../Styles/Colors';
import InputNormal from '../../Components/InputNormal';
import Font from '../../../Styles/Fonts';
import {Input} from 'react-native-elements/dist/input/Input';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {screenWidth} from '../../../Variable';
import BtnPrimary from '../../Components/BtnPrimary';

const index = ({navigation}) => {
  const inputIcon = (label, title, val) => {
    return (
      <View style={{marginTop: 16}}>
        <Text style={Font.bold12}>{label}</Text>
        <Input
          placeholder={title}
          style={[Font.reguler12, {color: Colors.black}]}
          inputContainerStyle={{borderBottomWidth: 0}}
          containerStyle={{
            height: 48,
            marginTop: 10,
            borderWidth: 1,
            borderRadius: 100,
            borderColor: Colors.container,
          }}
          value={val}
          rightIcon={() => (
            <FontAwesome5
              name={'chevron-down'}
              size={10}
              color={Colors.black}
            />
          )}
        />
      </View>
    );
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
      <ScrollView
        style={{marginHorizontal: 16}}
        showsVerticalScrollIndicator={false}>
        <InputNormal
          val={''}
          label={'Nama Penerima'}
          title={'Masukan nama penerima'}
        />
        <InputNormal val={''} label={'No Telpon'} title={'Masukan no telpon'} />
        {inputIcon('Provinsi', 'Pilih provinsi')}
        {inputIcon('Kabupaten/Kota', 'Pilih kabupaten/kota')}
        {inputIcon('Kecamatan', 'Pilih Kecamatan')}
        <InputNormal val={''} label={'Kode Pos'} title={'Masukan kode pos'} />
        <InputNormal
          val={''}
          label={'Detail alamat'}
          title={'Masukan kode pos'}
        />
        <View style={{marginTop: 16}}>
          <Text style={Font.bold12}>Pilh dengan peta</Text>
          <Image
            source={require('../../../Assets/images/maps.png')}
            style={{width: screenWidth - 32, height: 151, borderRadius: 7}}
          />
        </View>
        <View style={{height: 32}} />
      </ScrollView>
      <View style={styles.absoluteFooter1}>
        <View style={{flex: 1}}>
          <BtnPrimary
            title={'Simpan'}
            pressed={() => navigation.navigate('Home')}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

export default index;

const styles = StyleSheet.create({
  absoluteFooter1: {
    flexDirection: 'row',
    backgroundColor: Colors.white,
    padding: 16,
    elevation: 10,
  },
});
