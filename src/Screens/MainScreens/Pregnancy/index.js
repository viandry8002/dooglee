import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  ScrollView,
  Image,
} from 'react-native';
import React, {useState} from 'react';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Money from '../../Components/Money';

const dataBayi = [
  {
    id: 1,
    title: '1 Bulan',
  },
  {
    id: 2,
    title: '2 Bulan',
  },
  {
    id: 3,
    title: '3 Bulan',
  },
  {
    id: 4,
    title: '4 Bulan',
  },
  {
    id: 5,
    title: '5 Bulan',
  },
  {
    id: 6,
    title: '6 Bulan',
  },
  {
    id: 7,
    title: '7 Bulan',
  },
  {
    id: 7,
    title: '7 Bulan',
  },
  {
    id: 8,
    title: '8 Bulan',
  },
  {
    id: 9,
    title: '9 Bulan',
  },
  {
    id: 10,
    title: '11 Bulan',
  },
  {
    id: 12,
    title: '12 Bulan',
  },
];

const dataProduct = [
  {
    id: 1,
    image: require('../../../Assets/images/product1.png'),
    title: 'Dooglee - Newborn Pillow Dooglee',
    price: 599000,
    disc: 700000,
  },
  {
    id: 2,
    image: require('../../../Assets/images/product2.png'),
    title: 'Dooglee - Newborn Pillow Dooglee',
    price: 599000,
    disc: 700000,
  },
  {
    id: 3,
    image: require('../../../Assets/images/product2.png'),
    title: 'Dooglee - Newborn Pillow Dooglee',
    price: 599000,
    disc: 700000,
  },
];

const index = () => {
  const [month, setMonth] = useState(1);

  const renderItem = ({item}) => {
    return (
      <View style={{flex: 1}}>
        <TouchableOpacity
          style={{
            width: 74,
            height: 40,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: Colors.white,
          }}
          onPress={() => setMonth(item.id)}>
          <Text
            style={[
              Font.bold12,
              month === item.id
                ? {color: Colors.primary}
                : {color: Colors.placeholder},
            ]}>
            {item.title}
          </Text>
        </TouchableOpacity>
        <View
          style={
            month === item.id
              ? {
                  borderBottomColor: Colors.primary,
                  borderBottomWidth: 1,
                }
              : {
                  borderBottomColor: Colors.placeholder,
                  borderBottomWidth: 0.5,
                }
          }
        />
      </View>
    );
  };

  const renderProduct = ({item}) => {
    return (
      <TouchableOpacity
        style={{
          marginHorizontal: 4,
          borderRadius: 15,
          height: 256,
          width: 167,
          backgroundColor: Colors.white,
          elevation: 4,
          flex: 0.5,
        }}>
        <Image
          source={item.image}
          style={{
            width: '100%',
            height: 167,
            borderTopLeftRadius: 15,
            borderTopRightRadius: 15,
            resizeMode: 'cover',
          }}
        />
        <View style={{flex: 1, margin: 8, justifyContent: 'space-between'}}>
          <Text style={Font.reguler12}>{item.title}</Text>
          <Money
            value={item.price}
            styling={[Font.bold14, {color: Colors.primary}]}
          />
          <Money
            value={item.disc}
            styling={[
              Font.reguler12,
              {
                color: Colors.placeholder,
                textDecorationStyle: 'solid',
                textDecorationLine: 'line-through',
              },
            ]}
          />
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={{height: 43}}>
        <FlatList
          data={dataBayi}
          renderItem={renderItem}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          // style={{position: 'absolute'}}
        />
      </View>
      <ScrollView
        style={{backgroundColor: Colors.white}}
        showsVerticalScrollIndicator={false}>
        <View style={{height: 16}} />
        <View style={{alignItems: 'center'}}>
          <Image
            source={require('../../../Assets/images/janin.png')}
            style={{
              width: 100,
              height: 100,
            }}
          />
          <Text style={Font.reguler12}>Usia Janin</Text>
          <View style={{height: 8}} />
          <Text style={[Font.bold16, {color: Colors.primary}]}>
            {' '}
            {month} Bulan
          </Text>
        </View>
        <View style={{height: 16}} />
        <View
          style={{
            marginHorizontal: 16,
            backgroundColor: Colors.white,
            elevation: 10,
            borderRadius: 15,
            flex: 1,
            padding: 16,
          }}>
          <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
              <Text style={Font.bold14}>Berat</Text>
              <Text style={[Font.bold16, {color: Colors.primary}]}>80-110</Text>
              <Text style={[Font.reguler12, {color: Colors.placeholder}]}>
                Gram
              </Text>
            </View>
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
              <Text style={Font.bold14}>Panjang</Text>
              <Text style={[Font.bold16, {color: Colors.primary}]}>
                10.6 - 12
              </Text>
              <Text style={[Font.reguler12, {color: Colors.placeholder}]}>
                Cm
              </Text>
            </View>
          </View>
          <View style={{height: 8}} />
          <Text style={[Font.bold14]}>Perkembangan Si Kecil</Text>
          <View style={{height: 8}} />
          <Text style={[Font.reguler12, {color: Colors.placeholder}]}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Est quisque
            cursus vulputate aliquam eu proin amet sed. Massa, venenatis felis
            turpis id sit consectetur. Neque sit sit nisl proin eu amet, lectus
            enim ante. Dui, fames at nec mi augue urna a. Imperdiet ultricies
            diam neque, sodales vulputate elementum maecenas porta. Risus
            euismod et tellus nibh in id lobortis at molestie. Pharetra cursus
            a, neque ultrices feugiat rutrum imperdiet. Placerat eu dui aliquam
            ultricies molestie pretium, risus. Hendrerit dictum aliquet platea
            pellentesque curabitur condimentum viverra. Et, volutpat morbi
            elementum aliquam tempor cras mauris urna. Convallis vestibulum, ac
            egestas ipsum. Eget habitant orci, egestas mattis viverra.
            Scelerisque.
          </Text>
        </View>
        <View style={{height: 16}} />
        <View style={styles.TitleTwo}>
          <Text style={Font.bold14}>Produk Rekomendasi Untuk Moms</Text>
          <Text
            style={[Font.bold12, {color: Colors.primary}]}
            onPress={() => {}}>
            Lihat Semua{' '}
            <AntDesign name="right" size={10} color={Colors.primary} />
          </Text>
        </View>
        <View style={{height: 16}} />
        <FlatList
          data={dataProduct}
          renderItem={renderProduct}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          style={{
            paddingLeft: 12,
            height: 270,
          }}
          ListFooterComponent={<View style={{width: 24}} />}
        />
      </ScrollView>
    </SafeAreaView>
  );
};

export default index;

const styles = StyleSheet.create({
  TitleTwo: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 16,
  },
});
