import React from 'react';
import {
  FlatList,
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  StatusBar,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import {screenWidth} from '../../../Variable';

const dataChild = [
  {
    id: 1,
    image: require('../../../Assets/images/boy.png'),
    title: 'ren',
    usia: 'Usia 7 Minggu 2 Hari',
  },
  {
    id: 2,
    image: require('../../../Assets/images/girl.png'),
    title: 'kayla',
    usia: 'Lahir 8 April 2022',
  },
];

const index = ({navigation}) => {
  const order = (title, img) => {
    return (
      <TouchableOpacity
        style={{alignItems: 'center'}}
        onPress={() => navigation.navigate('ListAlamat')}>
        <Image source={img} style={{width: 48, height: 48, marginBottom: 8}} />
        <Text style={Font.caption}>{title}</Text>
      </TouchableOpacity>
    );
  };

  const renderChild = ({item}) => {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginTop: 16,
          alignItems: 'center',
        }}>
        <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
          <View
            style={{
              backgroundColor: Colors.lightPrimary,
              borderRadius: 15,
              width: 48,
              height: 48,
              justifyContent: 'center',
              alignItems: 'center',
              marginRight: 8,
            }}>
            <Image source={item.image} style={{width: 36, height: 36}} />
          </View>
          <View>
            <Text style={Font.bold12}>My Baby</Text>
            <Text style={Font.caption}>Usia 7 Minggu 2 Hari</Text>
          </View>
        </View>

        <TouchableOpacity style={{flexDirection: 'row', alignItems: 'center'}}>
          <MaterialCommunityIcons
            name="pencil"
            size={13}
            color={Colors.primary}
          />
          <Text
            style={[Font.reguler12, {color: Colors.primary, marginLeft: 8}]}>
            Edit
          </Text>
        </TouchableOpacity>
      </View>
    );
  };

  const generalBtn = (title, img) => {
    return (
      <TouchableOpacity onPress={() => navigation.navigate('MetodePembayaran')}>
        <View style={{flexDirection: 'row', alignItems: 'center', height: 40}}>
          <Image source={img} style={{width: 24, height: 24, marginRight: 8}} />
          <Text style={Font.reguler12}>{title}</Text>
        </View>
        <View
          style={{
            marginTop: 8,
            borderBottomColor: Colors.blackShading,
            borderBottomWidth: 0.5,
          }}
        />
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: Colors.white}}>
      <StatusBar
        backgroundColor="transparent"
        translucent
        barStyle="light-content"
      />
      <Image
        source={require('../../../Assets/images/bgProfile.png')}
        style={{
          height: 218,
          width: screenWidth,
          position: 'absolute',
        }}
      />

      <ScrollView showsVerticalScrollIndicator={false}>
        <View
          style={{
            alignItems: 'center',
            height: 210,
          }}>
          <View style={{height: 60}} />
          <Image
            source={require('../../../Assets/images/dokter.png')}
            style={{width: 73, height: 73}}
          />
          <View style={{height: 8}} />
          <Text style={[Font.bold16, {color: Colors.white}]}>
            Viandry Prasiswandava
          </Text>
          <View style={{height: 8}} />
          <TouchableOpacity
            style={{
              width: 110,
              height: 26,
              backgroundColor: Colors.whiteShading,
              borderRadius: 100,
              alignItems: 'center',
              justifyContent: 'center',
              flexDirection: 'row',
            }}
            onPress={() => navigation.navigate('UpdateProfile')}>
            <MaterialCommunityIcons
              name="pencil"
              size={13}
              color={Colors.white}
            />
            <Text
              style={[Font.reguler12, {color: Colors.white, marginLeft: 8}]}>
              Edit Profile
            </Text>
          </TouchableOpacity>
        </View>
        {/* <ScrollView style={{backgroundColor: 'red'}}> */}
        {/* body */}
        <View style={{flex: 1, alignItems: 'center'}}>
          {/* elevation1 */}
          <View style={styles.wrapElevation}>
            <View style={styles.TitleTwo}>
              <Text style={Font.bold16}>Pesanan Saya</Text>
              <Text
                style={[Font.bold12, {color: Colors.primary}]}
                onPress={() => navigation.navigate('PesananSaya')}>
                Lihat Semua{' '}
                <AntDesign name="right" size={10} color={Colors.primary} />
              </Text>
            </View>
            <View style={{height: 16}} />
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              {order(
                'Lanjut Pembayaran',
                require('../../../Assets/icons/credit.png'),
              )}
              {order(
                'Sedang Dikemas',
                require('../../../Assets/icons/box.png'),
              )}
              {order(
                'Dalam Pengiriman',
                require('../../../Assets/icons/truck.png'),
              )}
            </View>
          </View>
          {/* elevation2 */}
          <View style={styles.wrapElevation}>
            <View style={styles.TitleTwo}>
              <Text style={Font.bold16}>Anakku</Text>
              <Text
                style={[Font.bold12, {color: Colors.primary}]}
                onPress={() =>
                  navigation.push('UserPregnancy', {type: 'Profile'})
                }>
                <AntDesign name="pluscircle" size={12} color={Colors.primary} />{' '}
                Tambah Anak
              </Text>
            </View>
            <FlatList data={dataChild} renderItem={renderChild} />
          </View>
          {/* elevation3 */}
          <View style={styles.wrapElevation}>
            <View style={styles.TitleTwo}>
              <Text style={Font.bold16}>General</Text>
            </View>
            <View style={{height: 16}} />

            {generalBtn(
              'Tentang Kami',
              require('../../../Assets/icons/twoPerson.png'),
            )}
            {generalBtn(
              'Hubungi Kami',
              require('../../../Assets/icons/Message_fill.png'),
            )}
            {generalBtn(
              'Syarat & Ketentuan',
              require('../../../Assets/icons/File_dock_fill.png'),
            )}
            {generalBtn(
              'Kebijakan Privasi',
              require('../../../Assets/icons/Chield_check_fill.png'),
            )}
            {generalBtn(
              'Sign Out',
              require('../../../Assets/icons/Sign_out.png'),
            )}
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default index;

const styles = StyleSheet.create({
  TitleTwo: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  wrapElevation: {
    width: screenWidth - 16,
    backgroundColor: Colors.white,
    borderRadius: 15,
    elevation: 10,
    // top: 210,
    padding: 16,
    marginBottom: 16,
  },
});
