import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  FlatList,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import Colors from '../../../Styles/Colors';
import {screenWidth} from '../../../Variable';
import Font from '../../../Styles/Fonts';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {Input} from 'react-native-elements';

const dataBayi = [
  {
    id: 1,
    title: 'Bantal',
  },
  {
    id: 2,
    title: 'Bantal',
  },
  {
    id: 3,
    title: 'Bantal',
  },
  {
    id: 4,
    title: 'Bantal',
  },
  {
    id: 5,
    title: 'Bantal',
  },
  {
    id: 6,
    title: 'Bantal',
  },
];

const index = ({navigation}) => {
  const [focused, setFocused] = useState(true);

  const renderItem = ({item}) => {
    return (
      <View
        style={{
          flexDirection: 'row',
          flex: 1,
          justifyContent: 'space-between',
          marginVertical: 4,
          marginHorizontal: 16,
        }}>
        <Text style={Font.reguler12}>{item.title}</Text>
        <TouchableOpacity>
          <Image
            source={require('../../../Assets/icons/Close_round.png')}
            style={{width: 24, height: 24}}
          />
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={{height: 44, backgroundColor: Colors.white}} />

      <View
        style={{
          height: 50,
          alignItems: 'center',
          flexDirection: 'row',
          backgroundColor: Colors.white,
        }}>
        <AntDesign
          name="left"
          size={16}
          color={Colors.primary}
          style={{marginLeft: 16, marginRight: 8}}
          onPress={() => navigation.goBack()}
        />

        <Input
          placeholder={'Cari Sesuatu...'}
          style={[Font.reguler12, {color: Colors.black}]}
          inputContainerStyle={{
            borderBottomWidth: 0,
            height: 34,
          }}
          containerStyle={{
            height: 34,
            width: screenWidth - 70,
            borderRadius: 100,
            backgroundColor: Colors.container,
          }}
          leftIcon={
            <Image
              source={require('../../../Assets/icons/search.png')}
              style={{width: 18, height: 18}}
            />
          }
          // value={val}
          autoFocus={focused}
          returnKeyType={'search'}
        />
        {/* <TouchableOpacity
          onPress={() => navigation.navigate('SearchProduct')}
          style={{
            backgroundColor: Colors.container,
            height: 34,
            borderRadius: 100,
            width: screenWidth - 70,
            paddingHorizontal: 8,
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <Image
            source={require('../../../Assets/icons/search.png')}
            style={{width: 18, height: 18}}
          />
          <Text
            style={[
              Font.reguler12,
              {color: Colors.placeholder, marginLeft: 16},
            ]}>
            Cari Sesuatu...
          </Text>
        </TouchableOpacity> */}
      </View>
      <View style={{flex: 1}}>
        <FlatList
          data={dataBayi}
          renderItem={renderItem}
          style={{marginTop: 4}}
          ListFooterComponent={
            <Text style={[Font.reguler12, {textAlign: 'center'}]}>
              Hapus riwayat pencarian
            </Text>
          }
        />
      </View>
    </SafeAreaView>
  );
};

export default index;

const styles = StyleSheet.create({});
