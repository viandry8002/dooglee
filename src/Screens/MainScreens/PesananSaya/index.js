import {
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  FlatList,
  Image,
} from 'react-native';
import React, {useState} from 'react';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import Money from '../../Components/Money';
import StatusBarDC from '../../Components/StatusBarDC';
import BtnPrimary from '../../Components/BtnPrimary';

const datafilter = [
  {
    id: 1,
    title: 'Belum Bayar',
  },
  {
    id: 2,
    title: 'Diproses',
  },
  {
    id: 3,
    title: 'Dikirim',
  },
  {
    id: 4,
    title: 'Selesai',
  },
  {
    id: 5,
    title: 'Dibatalkan',
  },
];

const dataProduct = [
  {
    id: 1,
    date: '27 Nov 202, 20:30',
    title: 'Topi Bayi',
    jml: '1',
    total: '123456',
    status: 'Belum Bayar',
  },
  {
    id: 2,
    date: '27 Nov 202, 20:30',
    title: 'Topi Bayi',
    jml: '1',
    total: '123456',
    status: 'Belum Bayar',
  },
  {
    id: 3,
    date: '27 Nov 202, 20:30',
    title: 'Topi Bayi',
    jml: '1',
    total: '123456',
    status: 'Belum Bayar',
  },
];

const index = ({navigation}) => {
  const [chose, setChose] = useState(1);

  const renderFilter = ({item}) => {
    return (
      <View style={{flex: 1}}>
        <TouchableOpacity
          style={{
            flex: 1,
            backgroundColor: Colors.white,
            paddingHorizontal: 16,
            alignItems: 'center',
            justifyContent: 'center',
          }}
          onPress={() => setChose(item.id)}>
          {chose === item.id ? (
            <Text style={[Font.bold12, {color: Colors.primary}]}>
              {item.title}
            </Text>
          ) : (
            <Text style={[Font.reguler12, {color: Colors.placeholder}]}>
              {item.title}
            </Text>
          )}
        </TouchableOpacity>
        <View
          style={
            chose === item.id
              ? {
                  borderBottomColor: Colors.primary,
                  borderBottomWidth: 1,
                }
              : {
                  borderBottomWidth: 0.5,
                }
          }
        />
      </View>
    );
  };

  const horizontal = () => {
    return (
      <View
        style={{
          borderBottomColor: Colors.blackShading,
          borderBottomWidth: 0.5,
          marginVertical: 8,
        }}
      />
    );
  };

  const primaryBtn = () => {
    switch (chose) {
      case 1:
        return (
          <TouchableOpacity style={styles.btnPrimary} onPress={() => {}}>
            <Text style={[Font.bold12, {color: Colors.white}]}>
              Lanjut Pembayaran
            </Text>
          </TouchableOpacity>
        );
      case 2:
        return (
          <TouchableOpacity style={styles.btnPrimary} onPress={() => {}}>
            <Text style={[Font.bold12, {color: Colors.white}]}>
              Hubungi Penjual
            </Text>
          </TouchableOpacity>
        );
      case 3:
        return (
          <TouchableOpacity style={styles.btnPrimary} onPress={() => {}}>
            <Text style={[Font.bold12, {color: Colors.white}]}>
              Terima Pesanan
            </Text>
          </TouchableOpacity>
        );
      case 4:
        return (
          <TouchableOpacity style={styles.btnPrimary} onPress={() => {}}>
            <Text style={[Font.bold12, {color: Colors.white}]}>Beli lagi</Text>
          </TouchableOpacity>
        );
      case 5:
        return (
          <TouchableOpacity style={styles.btnPrimary} onPress={() => {}}>
            <Text style={[Font.bold12, {color: Colors.white}]}>Beli lagi</Text>
          </TouchableOpacity>
        );

      default:
        break;
    }
  };

  const renderProduct = ({item}) => {
    return (
      <View
        style={{
          flex: 1,
          //   height: 186,
          backgroundColor: Colors.white,
          marginBottom: 8,
          padding: 16,
        }}>
        {/* header */}
        <View style={{flexDirection: 'row', alignItems: 'center', flex: 1}}>
          <Image
            source={require('../../../Assets/icons/belanja.png')}
            style={{width: 24, height: 24, marginRight: 8}}
          />
          <View>
            <Text style={Font.bold12}>Belanja</Text>
            <Text style={[Font.caption, {color: Colors.placeholder}]}>
              {item.date}
            </Text>
          </View>
        </View>
        {horizontal()}
        {/* body */}
        <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
          <Image
            source={require('../../../Assets/images/product1.png')}
            style={{width: 56, height: 56, borderRadius: 15, marginRight: 8}}
          />
          <View>
            <Text style={Font.bold12}>Topi Bayi</Text>
            <Text style={[Font.caption, {color: Colors.placeholder}]}>
              1 Barang
            </Text>
          </View>
        </View>
        {horizontal()}
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <View>
            <Text style={[Font.caption, {color: Colors.placeholder}]}>
              Total Pesanan
            </Text>
            <Money value={item.total} styling={Font.bold14} />
          </View>
          {primaryBtn()}
        </View>
      </View>
    );
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <StatusBarDC />
      <View style={{height: 40}}>
        <FlatList
          data={datafilter}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          renderItem={renderFilter}
        />
      </View>
      <FlatList
        showsVerticalScrollIndicator={false}
        data={dataProduct}
        renderItem={renderProduct}
        ListFooterComponent={<View style={{height: 24}} />}
      />
    </SafeAreaView>
  );
};

export default index;

const styles = StyleSheet.create({
  btnPrimary: {
    backgroundColor: Colors.primary,
    height: 34,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 100,
    padding: 8,
  },
});
