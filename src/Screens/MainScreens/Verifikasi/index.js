import OTPInputView from '@twotalltotems/react-native-otp-input';
import React from 'react';
import {Image, SafeAreaView, StyleSheet, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Colors from '../../../Styles/Colors';
import Font from '../../../Styles/Fonts';
import BtnPrimary from '../../Components/BtnPrimary';
import StatusBarDC from '../../Components/StatusBarDC';
// import * as Animatable from 'react-native-animatable';

const index = ({navigation}) => {
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: Colors.secondary}}>
      <StatusBarDC />
      <View style={{flex: 1}}>
        <View style={{height: 44}} />
        <View style={{marginHorizontal: 16}}>
          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity style={styles.touchBack}>
              <AntDesign name="left" size={16} color={Colors.primary} />
            </TouchableOpacity>
            <Image
              source={require('../../../Assets/images/Phone.png')}
              style={{width: 174, height: 170, marginTop: 16}}
              resizeMode={'contain'}
            />
          </View>
        </View>
        <View style={{height: 16}} />
        {/* body */}
        <View style={styles.wrapBody}>
          <Text style={Font.bold16}>Verifikasi OTP</Text>
          <View style={{height: 16}} />
          <Text style={[Font.reguler12, {color: Colors.placeholder}]}>
            Kami telah mengirimkan 5 Digit kode ke nomor{' '}
            <Text style={[Font.bold12, {color: Colors.primary}]}>
              0823-3123-3232
            </Text>{' '}
            Masukan kode tersebut untuk melanjutkan ke aplikasi
          </Text>
          <View style={{height: 16}} />
          <OTPInputView
            style={{width: 227, height: 53}}
            pinCount={5}
            autoFocusOnLoad
            codeInputFieldStyle={[
              Font.bold16,
              {
                width: 39,
                height: 53,
                backgroundColor: Colors.container,
                borderWidth: 0,
                borderRadius: 7,
              },
              // success === false ? {
              //     borderWidth: 1,
              //     borderColor: colors.red
              // } : false
            ]}
            codeInputHighlightStyle={Colors.primary}
            // onCodeChanged={code => {
            //   setOtp(code);
            // }}
          />
          <View style={{height: 16}} />
          <Text style={[Font.reguler12, {color: Colors.placeholder}]}>
            Belum menerima Kode ?{' '}
            <Text style={[Font.bold12, {color: Colors.primary}]}>
              Kirim lagi (12d)
            </Text>
          </Text>
        </View>
        <View style={styles.absoluteFooter}>
          <BtnPrimary
            title={'Verifikasi'}
            pressed={() =>
              navigation.navigate('UserPregnancy', {type: 'Regist'})
            }
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

export default index;

const styles = StyleSheet.create({
  wrapBody: {
    flex: 1,
    backgroundColor: Colors.white,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    padding: 16,
  },
  absoluteFooter: {
    backgroundColor: Colors.white,
    padding: 16,
    elevation: 10,
  },
  touchBack: {
    backgroundColor: Colors.white,
    width: 36,
    height: 36,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 49,
    marginTop: 8,
  },
});
