import React from 'react';
import {StyleSheet} from 'react-native';
import Colors from './Colors';

const Font = StyleSheet.create({
  caption: {
    fontSize: 11,
    color: Colors.black,
    fontFamily: 'Nunito-Regular',
  },
  bold12: {
    fontSize: 12,
    color: Colors.black,
    fontFamily: 'Nunito-SemiBold',
  },
  reguler12: {
    fontSize: 12,
    color: Colors.black,
    fontFamily: 'Nunito-Regular',
  },
  bold14: {
    fontSize: 14,
    color: Colors.black,
    fontFamily: 'Nunito-SemiBold',
  },
  reguler14: {
    fontSize: 14,
    color: Colors.black,
    fontFamily: 'Nunito-Regular',
  },
  reguler16: {
    fontSize: 16,
    color: Colors.black,
    fontFamily: 'Nunito-Regular',
  },
  bold16: {
    fontSize: 16,
    color: Colors.black,
    fontFamily: 'Nunito-SemiBold',
  },
  bold24: {
    fontSize: 24,
    color: Colors.black,
    fontFamily: 'Nunito-Bold',
  },
});

export default Font;
