const Colors = {
  primary: '#1F8297',
  primaryShading: 'rgba(31, 130, 151, 0.3)',
  lightPrimary: '#E9F3F5',
  secondary: '#F3F2F3',
  placeholder: '#79747E',
  white: '#FFFFFF',
  whiteShading: 'rgba(255, 255, 255, 0.1)',
  container: '#F2F2F2',
  success: '#12B76A',
  black: '#000000',
  blackShading: 'rgba(0, 0, 0, 0.2)',
  gray1: '#333333',
  gray2: '#4F4F4F',
  error: '#EF3A3A',
};

export default Colors;
